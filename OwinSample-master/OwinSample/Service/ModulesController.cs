﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OwinSample;
using System.Web.Http.Cors;

namespace OwinSample.Service
{
    /// <summary>
    /// Moduliu valdymo klase
    /// </summary>
    [EnableCors(origins: "http://localhost", headers: "*", methods: "*")]
    public class ModulesController : ApiController
    {
        private CSSDatabaseEntities db = new CSSDatabaseEntities();

        /// <summary>
        /// Metodas grazinantis visus modulius
        /// </summary>
        /// <returns></returns>
        // GET: api/Modules
        [Authorize]
        public IQueryable<Module> IGetModule()
        {
            return db.Module;
        }
        /// <summary>
        /// Metodas grazinantis moduli pagal id
        /// </summary>
        /// <param name="id"> modulio id</param>
        /// <returns></returns>
        [Authorize]
        // GET: api/Modules/5
        [ResponseType(typeof(Module))]
        public IHttpActionResult GetModule(int id)
        {
            Module module = db.Module.Find(id);
            if (module == null)
            {
                return NotFound();
            }

            return Ok(module);
        }
        /// <summary>
        /// Metodas atnaujinantis moduli
        /// </summary>
        /// <param name="id">modulio id</param>
        /// <param name="module">modulio objektas</param>
        /// <returns></returns>
        [Authorize]
        // PUT: api/Modules/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutModule(int id, Module module)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != module.ModuleID)
            {
                return BadRequest();
            }

            db.Entry(module).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModuleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(module);
        }
        /// <summary>
        /// Metodas issaugantis moduli
        /// </summary>
        /// <param name="module">modulio objektas</param>
        /// <returns></returns>
        [Authorize]
        // POST: api/Modules
        [ResponseType(typeof(Module))]
        public IHttpActionResult PostModule(Module module)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Module.Add(module);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = module.ModuleID }, module);
        }
        /// <summary>
        /// metodas istrinantis moduli pagal id
        /// </summary>
        /// <param name="id">modulio id</param>
        /// <returns></returns>
        [Authorize]
        // DELETE: api/Modules/5
        [ResponseType(typeof(Module))]
        public IHttpActionResult DeleteModule(int id)
        {
            Module module = db.Module.Find(id);
            if (module == null)
            {
                return NotFound();
            }

            db.Module.Remove(module);
            db.SaveChanges();

            return Ok(module);
        }
        /// <summary>
        /// metodas uzbaigimo
        /// </summary>
        /// <param name="disposing"></param>
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        /// <summary>
        /// metodas tikrina ar modulis egzistuoja
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        private bool ModuleExists(int id)
        {
            return db.Module.Count(e => e.ModuleID == id) > 0;
        }
    }
}