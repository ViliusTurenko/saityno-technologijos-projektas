﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OwinSample;
using System.Web.Http.Cors;

namespace OwinSample.Service
{
    /// <summary>
    /// vartotoju valdymo klase
    /// </summary>
    [EnableCors(origins: "http://localhost", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {
        private CSSDatabaseEntities db = new CSSDatabaseEntities();
        /// <summary>
        /// Metodas grazinantis vartotoja pagal el pasta ir slaptazodi
        /// </summary>
        /// <param name="email"> el pastas</param>
        /// <param name="password"> slaptazodis</param>
        /// <returns></returns>
        [AllowAnonymous]
        public User GGetUserByCredentials(string email, string password)
        {

            bool exists = (from d in db.User where d.Username == email && d.Password == password select new { d.Email, d.Password }).Any();
            
               if (!exists) 
            {
                return null;
            }
            User user = new User() { UserID = 1, Email = email, Password = password, Name = "Vilius" };
            if (user != null)
            {
                user.Password = string.Empty;
            }
            return user;
        }
        /// <summary>
        /// Metodas grazinantis vartotojus
        /// </summary>
        /// <returns></returns>
        // GET: api/Users
        public IQueryable<User> AGetUser()
        {
            return db.User;
        }
        /// <summary>
        /// MEtodas grazinantis vartotoja pagal id
        /// </summary>
        /// <param name="id">vartotojo id</param>
        /// <returns></returns>
        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(int id)
        {
            User user = db.User.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }
        /// <summary>
        /// Metodas atnaujinantis vartotoja pagal id
        /// </summary>
        /// <param name="id">vartotojo id</param>
        /// <param name="user">vartotojo objektas</param>
        /// <returns></returns>
        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserID)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        /// <summary>
        /// Metodas pridedantis vartotoja
        /// </summary>
        /// <param name="user"> vartotojo objektas</param>
        /// <returns></returns>
        [AllowAnonymous]
        // POST: api/Users
        [ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {
            user.Points = 50;
            user.CreatedDate = System.DateTime.Now;
            user.UserType = "client";
        bool exists = (from d in db.User where d.Username == user.Username || d.Email == user.Email select new { d.Email, d.Password }).Any();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (!exists) { 
            db.User.Add(user);
            db.SaveChanges();
           
            return CreatedAtRoute("DefaultApi", new { id = user.UserID }, user);
            }
            return null;
        }
        /// <summary>
        /// metodas istrinantis vartotoja pagal id
        /// </summary>
        /// <param name="id">vartotojo id</param>
        /// <returns></returns>
        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = db.User.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.User.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }
        /// <summary>
        /// uzbaigimo metodas
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        /// <summary>
        /// metodas tikrinantis ar vartotojas egzistuoja
        /// </summary>
        /// <param name="id">vartotojo id</param>
        /// <returns></returns>
        private bool UserExists(int id)
        {
            return db.User.Count(e => e.UserID == id) > 0;
        }
    }
}