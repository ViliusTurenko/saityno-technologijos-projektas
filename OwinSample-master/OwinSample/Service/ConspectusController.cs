﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OwinSample;
using System.Web.Http.Cors;

namespace OwinSample.Service
{
    /// <summary>
    /// Konspektu valdymo api klase
    /// </summary>
    [EnableCors(origins: "http://localhost", headers: "*", methods: "*")]
    public class ConspectusController : ApiController
    {
    
        private CSSDatabaseEntities db = new CSSDatabaseEntities();
        /// <summary>
        /// Metodas grazinantis visus konspektus
        /// </summary>
        /// <returns></returns>
        [Authorize]
        // GET: api/Conspectus
        public IQueryable<Conspectus> IGetConspectus()
        {
            return db.Conspectus;
        }
        /// <summary>
        /// Metodas grazinantis konspektą pagal ID
        /// </summary>
        /// <param name="id">Konspekto ID</param>
        /// <returns></returns>
        [Authorize]
        // GET: api/Conspectus/5
        [ResponseType(typeof(Conspectus))]
        public IHttpActionResult GetConspectus(int id)
        {
            Conspectus conspectus = db.Conspectus.Find(id);
            if (conspectus == null)
            {
                return NotFound();
            }
            conspectus.HtmlText = System.Web.HttpUtility.HtmlDecode(conspectus.HtmlText);
            return Ok(conspectus);
        }
        /// <summary>
        /// Metodas atnaujinantis konspektą pagal ID
        /// </summary>
        /// <param name="id">Konspekto ID</param>
        /// <param name="conspectus">Konspekto objektas</param>
        /// <returns></returns>
        [Authorize]
        // PUT: api/Conspectus/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutConspectus(int id, Conspectus conspectus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != conspectus.ConspectusID)
            {
                return BadRequest();
            }

            db.Entry(conspectus).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConspectusExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        /// <summary>
        /// Metodas issaugantis konspekta
        /// </summary>
        /// <param name="conspectus">Konspekto objektas</param>
        /// <returns></returns>
        [Authorize]
        // POST: api/Conspectus
        [ResponseType(typeof(Conspectus))]
        public IHttpActionResult PostConspectus(Conspectus conspectus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Conspectus.Add(conspectus);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = conspectus.ConspectusID }, conspectus);
        }
        /// <summary>
        /// Metodas istrinantis konspekta
        /// </summary>
        /// <param name="id">Konspekto ID</param>
        /// <returns></returns>
        [Authorize]
        // DELETE: api/Conspectus/5
        [ResponseType(typeof(Conspectus))]
        public IHttpActionResult DeleteConspectus(int id)
        {
            Conspectus conspectus = db.Conspectus.Find(id);
            if (conspectus == null)
            {
                return NotFound();
            }

            db.Conspectus.Remove(conspectus);
            db.SaveChanges();

            return Ok(conspectus);
        }
        /// <summary>
        /// Uzbaigimo metodas
        /// </summary>
        /// <param name="disposing"></param>
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        /// <summary>
        /// Tikrina ar konspektas egizstuoja
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        private bool ConspectusExists(int id)
        {
            return db.Conspectus.Count(e => e.ConspectusID == id) > 0;
        }
    }
}