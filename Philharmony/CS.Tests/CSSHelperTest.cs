// <copyright file="CSSHelperTest.cs">Copyright ©  2017</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Philharmony;

namespace Philharmony.Tests
{
    [TestClass]
    [PexClass(typeof(CSSHelper))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class CSSHelperTest
    {

        [PexMethod]
        [PexAllowedException(typeof(TypeInitializationException))]
        public void RegisterUser(string username, string password)
        {
            CSSHelper.RegisterUser(username, password);
            // TODO: add assertions to method CSSHelperTest.RegisterUser(String, String)
        }
    }
}
