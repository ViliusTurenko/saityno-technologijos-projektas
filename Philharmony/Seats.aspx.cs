﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{
    public partial class Seats : System.Web.UI.Page
    {
        public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["PhilharmonyConnectionString"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            TableRow[] rows = new TableRow[25];
            con.Open();
            SqlCommand cmd1 = new SqlCommand("web_HallSeat_SelectAll", con);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            da.Fill(dt);
           
            TableCell[] tc = new TableCell[680];
            TableRow[] tr = new TableRow[25];
            ImageButton[] ib = new ImageButton[680];

            int k = 1;
            int i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                int RowNumber = int.Parse(dr["SeatRow"].ToString());
                int SeatNumber = int.Parse(dr["SeatNumber"].ToString());
                tr[RowNumber] = new TableRow();
                tc[SeatNumber] = new TableCell();
                ib[i] = new ImageButton();
                ib[i].ID = "s" + RowNumber.ToString() + SeatNumber.ToString();
                if (dr["IsReserved"].ToString() == "false")
                {
                    ib[i].ToolTip = "Row: " + dr["SeatRow"].ToString() + " seat: " + dr["SeatNumber"].ToString() + " price: " + dr["Price"].ToString();
                    ib[i].ImageUrl = "~/img/available_seat_img.gif";
                }
                else
                {
                    ib[i].ToolTip = "Seat is occupied";
                    ib[i].ImageUrl = "~/img/booked_seat_img.gif";
                }
                tc[SeatNumber].Controls.Add(ib[i]);
                tr[RowNumber].Cells.Add(tc[SeatNumber]);
                seattable.Rows.Add(tr[RowNumber]);
            }
           // int id = 41;
           // int i = 0;
           /* List<Seat>[] seats = new List<Seat>[5];
            Seat seat = new Seat();
            seats[0] = new List<Seat>();
            seats[1] = new List<Seat>();
            seats[2] = new List<Seat>();
            seats[3] = new List<Seat>();
            seats[4] = new List<Seat>();
            foreach (DataRow dr in dt.Rows)
            {
                int RowNumber = (int)dr["SeatRow"];
                int SeatNumber = (int)dr["SeatNumber"];
                int HallGroupID = (int)dr["HallGroupID"];

                seat.IsReserved = (string)dr["IsReserved"];
                var PriceDec = (Decimal)dr["Price"];
                seat.Price = (float)PriceDec;
                seat.RowNumber = RowNumber;
                seat.SeatNumber = SeatNumber;
                seat.HallGroupID = HallGroupID;
                if (HallGroupID == id)
                {
                    seats[i].Add(seat);

                }
                else
                {
                    i++; id++;
                    seats[i].Add(seat);
                }
            }*/
     
               
            

        }

    }

}
