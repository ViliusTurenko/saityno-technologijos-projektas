﻿/*Registration.aspx.cs
2.v1.0
3.2017/06/08
4.2017/06/13
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{
    /// <summary>
    /// Class which holds methods for registration
    /// </summary>
    public partial class Registration : System.Web.UI.Page
    {
        /// <summary>
        /// Main registration page method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.Request.IsAuthenticated)
            {
                Response.Redirect("/Main.aspx");
            }

        }
    }
}