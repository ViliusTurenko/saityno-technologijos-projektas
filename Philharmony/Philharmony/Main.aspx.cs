﻿/*Reservation.aspx.cs
2.v1.0
3.2017/05/29
4.2017/06/13
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
namespace Philharmony
{
    /// <summary>
    /// Class for Main web form 
    /// </summary>
    public partial class Main : System.Web.UI.Page
    {
        public string Opr { get; set; }     //Variable for storing user selected operation(check, reserve)
        public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CSSDatabaseConnectionString"].ConnectionString);//Philharmony DB connection string

        /// <summary>
        /// Main page method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [WebMethod(EnableSession = true)]
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["Token"] = CSSHelper.tokenstring;

            if (!Page.Request.IsAuthenticated)
            {
                Response.Redirect("/Login.aspx");
            }
            else
            {
                Session["Userid"] = CSSHelper.GetUserID();
                hdnUserID.Value = string.IsNullOrEmpty(Session["Userid"].ToString()) ? Session["Userid"].ToString() : CSSHelper.GetUserID().ToString();
                DataGrid2.Visible = true;

                if (!Page.IsPostBack)
                {

                    BindModuleDDl();

                }



            }
        }
        protected void BindModuleDDl()
        {
            ddlModule.DataSource = CSSHelper.GetModuleData();
            ddlModule.DataValueField = "ModuleID";
            ddlModule.DataTextField = "ModuleName";
            ddlModule.DataBind();
            con.Close();


        }
        protected void BindGrid()
        {
            DataTable dt4 = CSSHelper.FeelingLucky(ddlModule.SelectedValue, Session["UserID"].ToString());
            if (dt4.Rows.Count > 0)
            {
                ConspectCounttxt.Text = dt4.Rows.Count.ToString();
                DataGrid2.DataSource = dt4;
                DataGrid2.DataBind();
                tableDatagrid.Style.Value = "display:contents; width:100%";
            }
            else
            {
                tableDatagrid.Style.Value = "display:none";
            }
            con.Close();
        }
        protected void _1_OnClick(object sender, EventArgs e)
        {
            CSSHelper.rateConspect(conspectusidhdn.Value, "1");
        }

        protected void LinkButton1_OnClick(object sender, EventArgs e)
        {
            CSSHelper.rateConspect(conspectusidhdn.Value, "2");
        }

        protected void LinkButton2_OnClick(object sender, EventArgs e)
        {
            CSSHelper.rateConspect(conspectusidhdn.Value, "3");
        }

        protected void LinkButton3_OnClick(object sender, EventArgs e)
        {
            CSSHelper.rateConspect(conspectusidhdn.Value, "4");
        }

        protected void LinkButton4_OnClick(object sender, EventArgs e)
        {
            CSSHelper.rateConspect(conspectusidhdn.Value, "5");
        }

        protected void check_OnClick(object sender, EventArgs e)
        {
            BindGrid();
        }
    }



}