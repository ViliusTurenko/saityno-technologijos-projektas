﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{/// <summary>
/// Class which holds methods for displaying seats
/// </summary>
    public partial class Login : System.Web.UI.Page
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CSSConnectionString"].ConnectionString);      //Philharmony database connection string

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.Request.IsAuthenticated)
            {
                Response.Redirect("/Main.aspx");
            }

        }

        protected void RegisterUser(object sender, EventArgs e)
        {
            if(CSSHelper.RegisterUser(add_Username.Text, add_password.Text)==-1)
            {
                txtError.Text = "Prisijungimas nepavyko";
            }
        }

    }
}