﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserConspects.aspx.cs" Inherits="Philharmony.UserConspects" %>

<%@ Register Src="BannerTop.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Šperinkis</title>
    <link href="favicon.ico" rel="Shortcut Icon" type="image/x-icon" />
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />

    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="mystyle.css">

    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style>
        .ajax__html_editor_extender_texteditor {
            background-color: white;
            color: black;
                width:100%;
        }

        TextBox1$HtmlEditorExtenderBehavior_ExtenderContainer {
            color: black;
        }

        #ajax__html_editor_extender_buttoncontainer {
            background: linear-gradient(to top right, #00ccff 0%, #006699 100%);
        }
        .containerClass .ajax__html_editor_extender_container
        {
            width: 100% !important;/*important is really important at here*/
        }
    </style>
</head>
<body style="font-family: 'Open Sans', sans-serif;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <header>
            <uc1:Header ID="WebUserControl1" runat="server" />
        </header>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div style="margin: 0px 1000px">
                    <asp:Image ID="Image1" ImageUrl="img/loader.gif" AlternateText="Processing" runat="server" Height="60px" Width="60px" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel UpdateMode="Always" ID="UpdatePanel1" runat="server">
            <ContentTemplate>


                <table class="TableBorder" cellspacing="1" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td style="padding-left: 2%">
                            <asp:TextBox runat="server" ID="ConspectCount"></asp:TextBox>
                            <asp:Label runat="server" Font-Size="24" ForeColor="white" Text="Mano konspektai"></asp:Label>
                            <asp:DataGrid ID="DataGrid1" style="width:95%; max-width:1000px; height:auto; /* w3c, ie10+, ff16+, chrome26+, opera12+, safari7+ *//* w3c, ie10+, ff16+, chrome26+, opera12+, safari7+ */ font-size: 16px;" CellPadding="3"
                                 AllowSorting="True" AutoGenerateColumns="False" GridLines="none" CellSpacing="1"
                                runat="server">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <ItemStyle Width="2%"></ItemStyle>
                                        
                                        <ItemTemplate>
                                              <asp:ImageButton runat="server"  Width="35" Height="35" ImageUrl="img/006-eraser.svg"   ToolTip="Ištrinti" OnClientClick='<%#"DeleteConspectus("+Eval("ConspectusID").ToString()+")"%>' ID="deleteButton" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemStyle Width="3%"></ItemStyle>
                                       
                                        <ItemTemplate>
                                    <asp:ImageButton  Width="35" Height="35" ImageUrl="img/005-pencil.svg"   runat="server" OnClientClick='<%#"editConspectus("+Eval("ConspectusID").ToString()+")"%>' ID="editButton" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="50%">
                                        <HeaderTemplate>
                                            Konspekto pavadinimas
                                        </HeaderTemplate>
                                        <ItemStyle CssClass="TableStyleCell1" />
                                        <ItemTemplate>
                                            <asp:LinkButton OnClientClick='<%#"OnConspectusClick("+Eval("ConspectusID")+")" %>' runat="server" ID="lbl_ConspectID" Text='<%# Eval("ConspectusName") %>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn >
                                    <asp:TemplateColumn ItemStyle-Width="10%">
                                        <HeaderTemplate>
                                            <%# "Reitingas" %>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRating" runat="server" ToolTip='<%#" Balsavo : "+Eval("RatedCount") %>' Text='<%# String.Format("{0:0.##}",Eval("Rating")) %>'>
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="10%">
                                        <HeaderTemplate>
                                            <%# "Kaina" %>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRating" runat="server" ToolTip='Taškų skaičius reikalingas peržiūrai' Text='<%# Eval("Price") %>'>
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Position="TopAndBottom" HorizontalAlign="Left" PageButtonCount="20"
                                    Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid></td>
                    </tr>
                </table>
                <br>

                <a name="Edit"></a>
                <table id="add_Table" cellspacing="1" cellpadding="0" width="100%"
                    border="0" runat="server">
                    <tr>
                        <td>

                            <table style="width:100%;">
                                <tr>
                                    <td align="left" colspan="2"><%= "Konspekto redagavimas/pridėjimas" %></td>
                                </tr>
                                <tr>
                                    <td id="tdConspectusID" style="display: none" align="right"><%= "Konspekto id :" %></td>
                                    <td>

                                        <asp:TextBox ID="ConspectusID" Enabled="false" Style="display: none; background-color: #FEFDEE; font-size: 11px; border: solid 1px #74BAF3; color: black;min-width:200px; width:20%;" runat="server" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="right"><%= "Konspekto pavadinimas" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;</td>
                                    <td>
                                        <asp:HiddenField ID="HdnConspectusID" runat="server" />
                                        <asp:TextBox ID="add_ConspectusName" Style="background-color: #FEFDEE; font-size: 11px; border: solid 1px #74BAF3; color: black; min-width:200px;width:20%" runat="server"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" CssClass="validationWarningSmall" runat="server" ForeColor=" "
                                            ErrorMessage="*" ControlToValidate="add_ConspectusName" Display="Static"></asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="right"><%= "Kaina" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;</td>
                                    <td>
                                        <asp:TextBox ID="add_Price" Style="background-color: #FEFDEE; font-size: 11px; border: solid 1px #74BAF3; color: black; min-width:200px; width:20%" runat="server"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="validationWarningSmall" runat="server" ForeColor=" "
                                            ErrorMessage="*" ControlToValidate="add_Price" Display="Static"></asp:RequiredFieldValidator></td>
                                </tr>

                                <tr>
                                    <td align="right"><%= "Modulis" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;</td>
                                    <td style="padding-top:10px;">
                                        <asp:DropDownList ID="Add_Module" runat="server" Style="color: black;  min-width:200px;width: 20%" ></asp:DropDownList>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="validationWarningSmall" runat="server" ForeColor=" "
                                            ErrorMessage="*" ControlToValidate="add_Price" Display="Static"></asp:RequiredFieldValidator></td>
                                </tr>

                                <tr >
                                                             <td align="right"><%= "Tekstas" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;</td>
                                    <td style=" width:100%;padding-bottom:10%; padding-right:2%;">
                                        <div class="containerClass">
                                        <asp:TextBox runat="server" style="width:100%; height:auto;  min-height:400px; max-height:800px;" ForeColor="black" ID="TextBox1"></asp:TextBox>
                                        <ajaxToolkit:HtmlEditorExtender OnImageUploadComplete="HtmlEditorExtender1_ImageUploadComplete"
                                            DisplaySourceTab="true" ID="HtmlEditorExtender1"
                                            TargetControlID="TextBox1"
                                            runat="server">
                                            <Toolbar>
                                                <ajaxToolkit:Undo />
                                                <ajaxToolkit:Redo />
                                                <ajaxToolkit:Bold />
                                                <ajaxToolkit:Italic />

                                                <ajaxToolkit:Underline />
                                                <ajaxToolkit:StrikeThrough />
                                                <ajaxToolkit:Subscript />
                                                <ajaxToolkit:Superscript />
                                                <ajaxToolkit:JustifyLeft />
                                                <ajaxToolkit:JustifyCenter />
                                                <ajaxToolkit:JustifyRight />
                                                <ajaxToolkit:JustifyFull />
                                                <ajaxToolkit:InsertOrderedList />
                                                <ajaxToolkit:InsertUnorderedList />
                                                <ajaxToolkit:CreateLink />
                                                <ajaxToolkit:UnLink />
                                                <ajaxToolkit:RemoveFormat />
                                                <ajaxToolkit:SelectAll />
                                                <ajaxToolkit:UnSelect />
                                                <ajaxToolkit:Delete />
                                                <ajaxToolkit:Cut />
                                                <ajaxToolkit:Copy />
                                                <ajaxToolkit:Paste />
                                                <ajaxToolkit:BackgroundColorSelector />
                                                <ajaxToolkit:ForeColorSelector />
                                                <ajaxToolkit:FontNameSelector />
                                                <ajaxToolkit:FontSizeSelector />
                                                <ajaxToolkit:Indent />
                                                <ajaxToolkit:Outdent />
                                                <ajaxToolkit:InsertHorizontalRule />
                                                <ajaxToolkit:HorizontalSeparator />
                                                <ajaxToolkit:InsertImage />
                                            </Toolbar>
                                        </ajaxToolkit:HtmlEditorExtender>
                                            </div>
                                    </td>
                                </tr>
                    </tr>

                    <tr>
                        <td align="right"><div id="MyDialog">
        </div></td>
                        <td>
                            <asp:HiddenField ID="hdnUserID" Value='<%#Session["Userid"] %>' runat="server" />
                        <asp:Button Width="70" Height="50"  id="btnSave" runat="server" OnClientClick='javascript:AddConspectus()' Tooltip='Išsaugoti'/>
                                                         <asp:ImageButton ID="Button"  Width="70" Height="50" ImageUrl="img/cancel.svg"
                                            OnClientClick="javascript:window.location.href='UserConspects.aspx'; return false;" runat="server"
                                            Tooltip='Atšaukti'></asp:ImageButton>
                            <asp:HiddenField ID="ConspectName" Value="" runat="server"/>
                            <asp:HiddenField ID="hdnToken" Value='<%#Session["Token"] %>' runat="server" />
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <hr />
        <footer>
            <uc1:Footer ID="footer1" runat="server" />
        </footer>
    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">

        $.ajaxSetup({
            beforeSend: function (xhr) {
                var h = document.getElementById('hdnToken');
                var token = h.value;
                xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            }
        });



        function DeleteConspectus(x) {
            var h = document.getElementById('hdnToken');
            var token = h.value;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("DELETE", "http://localhost:8080/api/Conspectus/" + x, false);
            xmlhttp.setRequestHeader('Authorization', 'Bearer ' + token);
            xmlhttp.send(null);
            var response = xmlhttp.responseText;
          
            location.reload();
        }

        function OnConspectusClick(x) {

            window.open('http://localhost/EditingPermissions.aspx?ConspectusID=' + x, 'newwin', 'height=800px,width=800px');

        }
        function editConspectus(x) {
            var h = document.getElementById('hdnToken');
            var token = h.value;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", "http://localhost:8080/api/Conspectus/" + x, false);
            xmlhttp.setRequestHeader('Authorization', 'Bearer ' + token);
            xmlhttp.send(null);
            var response = xmlhttp.responseText;
            var json = JSON.parse(response);
            var textbox = document.getElementById('HtmlEditorExtender1');
            var textbox2 = document.getElementById('add_ConspectusName');
            var textbox3 = document.getElementById('add_Price');
            var ConspectusID = document.getElementById('ConspectusID');
            var moduleidlbl = document.getElementById('tdConspectusID');
            var Conspect = document.getElementById('ConspectName');
            Conspect.value = "Fizikos pagrindai";
            var e = document.getElementById("Add_Module");
            moduleidlbl.style.display = "block";
            ConspectusID.style.display = "block";
            textbox2.value = json["ConspectusName"];
            textbox3.value = json["Price"];
            ConspectusID.value = json["ConspectusID"];
            e.options[e.selectedIndex].value = json["ModuleID"];
            var htmlEditorExtender = $('.ajax__html_editor_extender_texteditor');
            htmlEditorExtender.html(json["HtmlText"]);


        }

        $(document).ready(function () {
            $('#TextBox1$HtmlEditorExtenderBehavior_ExtenderContainer').removeAttr('style');
            alert($('#TextBox1$HtmlEditorExtenderBehavior_ExtenderContainer').css("width"));
            
        });
        function AddConspectus() {
            var UserIDhdn = document.getElementById('hdnUserID');
            var textbox = document.getElementById('TextBox1');
            var textbox2 = document.getElementById('add_ConspectusName');
            var textbox3 = document.getElementById('add_Price');
            var ConspectusIDt = document.getElementById('ConspectusID');
            var e = document.getElementById("Add_Module");
            var moduleid = e.options[e.selectedIndex].value;
            var Name = textbox2.value;
            var html = textbox.value;
            var Price = textbox3.value;
            if (!Name || !Price || !moduleid)
                return
            var ConspectusID = ConspectusIDt.value;
            var userid = UserIDhdn.value;
            var url = 'http://localhost:8080/api/Conspectus';
            if (ConspectusID) {
                var Conspectus = {
                    ConspectusID: ConspectusID,
                    Price: Price,
                    ConspectusName: Name,
                    ModuleID: moduleid,
                    HtmlText: html,
                    UserID: userid
                }

                $.ajax({
                    type: "PUT",
                    url: url + "/" + ConspectusID,
                    data: JSON.stringify(Conspectus),
                    contentType: "application/json; charset=utf-8",
                    success: onSuccess,
                    error: OnErrorCall
                });
            }
            else {
                var Conspectus = {
                    ConspectusName: Name,
                    ModuleID: moduleid,
                    Price: Price,
                    HtmlText: html,
                    UserID: userid
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify(Conspectus),
                    contentType: "application/json; charset=utf-8",
                    success: onSuccess,
                    error: OnErrorCall
                });
            }

            function onSuccess(response) {
                var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';

                if (UpdatePanel1 != null) {
                  
                }
            }
            function OnErrorCall(response) {
                alert(response);
            }
        }


        $.backstretch(
            [
                "img/4.jpg",
                "img/5.jpg",
                "img/6.jpg"

            ],

            {
                duration: 4500,
                fade: 1500
            }
        );

    </script>
</body>
</html>
