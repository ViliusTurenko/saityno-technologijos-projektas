﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="Philharmony.Registration" %>
<%@ Register Src="Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Šperinkis</title>
    <link href="favicon.ico" rel="Shortcut Icon" type="image/x-icon" />
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="style.css"/>

    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style>
        td{
            padding-bottom:1%;
        }
    </style>
</head>
<body style="font-family: 'Open Sans', sans-serif;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Šperinkis. Sharing - is caring</a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-right">

                        <li>
                                          <asp:LoginStatus ID="LoginStatus1" LoginImageUrl="img/sign-in.svg" Width="50"  Height="50" LogoutImageUrl="img/login (1).svg" runat="server" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                <div id="banner">
                    <h1>
                        <strong>Registracijos langas</strong><span class="icon-bar"></span></h1>
                </div>
            </div>
        </div>
        <asp:UpdatePanel ID="up" UpdateMode="Always" runat="server">
            <ContentTemplate>
                <div class="auto-style8" style="padding-left: 10%">
                    <table>
                        <tr>
                            <th colspan="3">Registracija
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="UsernameLabel" runat="server" Text="">Prisijungimo vardas</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox  ID="txtUsername" runat="server" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Privaloma" ForeColor="white" ControlToValidate="txtUsername"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="">Slaptažodis</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPassword" ForeColor="black" runat="server" TextMode="Password" />
                                  <asp:TextBox ID="lblError" ForeColor="black" runat="server" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Privaloma" ForeColor="white" ControlToValidate="txtPassword"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="">Patvirtinkite slaptažodį</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtConfirmPassword" ForeColor="black" runat="server" TextMode="Password" />
                            </td>
                            <td class="auto-style9">
                                <asp:CompareValidator ErrorMessage="Slaptažodžiai nesutampa" ForeColor="white" ControlToCompare="txtPassword"
                                    ControlToValidate="txtConfirmPassword" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text="">Vardas</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox  ID="Name" runat="server" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Privaloma" ForeColor="white" ControlToValidate="Name"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="">Pavardė</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="Lastname" runat="server" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Privaloma" ForeColor="white" ControlToValidate="Lastname"
                                    runat="server" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="Label5" runat="server" Text="">El paštas</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Privaloma" Display="Dynamic" ForeColor="white"
                                    ControlToValidate="txtEmail" runat="server" />
                                <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ControlToValidate="txtEmail" ForeColor="white" ErrorMessage="Blogas el pašto adresas." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server" Text="">Fakultetas</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFacoulty" runat="server" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Privaloma" Display="Dynamic" ForeColor="white"
                                    ControlToValidate="txtFacoulty" runat="server" />

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label8" runat="server" Text="">Kursas</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox  ID="txtCourse" runat="server" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Privaloma" Display="Dynamic" ForeColor="white"
                                    ControlToValidate="txtCourse" runat="server" />

                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                   <asp:ImageButton Width="70" Height="50" ImageUrl="img/icon.svg"   runat="server" OnClientClick='javascript:AddUser()' Tooltip='Registruotis'/>
                                                         <asp:ImageButton ID="Buttons"  Width="70" Height="50" ImageUrl="img/cancel.svg"
                                            OnClientClick="javascript:window.location.href='Registration.aspx'; return false;" runat="server"
                                            Tooltip='Atšaukti'></asp:ImageButton>
                               
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="LabelEmpty" runat="server" Font-Size="24px" Text=""></asp:Label>
                    <br />
                    <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Login.aspx" runat="server"></asp:HyperLink>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="auto-style7" id="divv2">
        </div>
    </form>
    <hr />
    <footer>
       
    </footer>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">

        function AddUser() {

            var txtbxUsername = document.getElementById('txtUsername');
            var txtbxPassword = document.getElementById('txtPassword');
            var txtbxName = document.getElementById('Name');
            var txtbxEMail = document.getElementById('txtEmail');
            var txtbxCourse = document.getElementById('txtCourse');
            var txtbxLastName = document.getElementById('Lastname');
            var txtbxFacoulty = document.getElementById('txtFacoulty');

            var Username = txtbxUsername.value;
            var Password = txtbxPassword.value;
            var name = txtbxName.value;
            var email = txtbxEMail.value;
            var course = txtbxCourse.value;
            var lastname = txtbxLastName.value;
            var facoulty = txtbxFacoulty.value;
            if (Username && Password && name && email && lastname && course) {
                var url = "http://localhost:8080/api/Users";

                var user = {
                    Username: Username,
                    Facoulty: facoulty,
                    Password: Password,
                    Name: name,
                    Email: email,
                    Course: course,
                    Lastname: lastname
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify(user),
                    contentType: "application/json; charset=utf-8",
                    success: onSuccess,
                    error: OnErrorCall
                });
            }
            else {
                alert('Neįvesti laukai!')
            }
        }

        function onSuccess(response) {
            $(location).attr('href', '/login.aspx')
            var UpdatePanel1 = '<%=up.ClientID%>';

            if (UpdatePanel1 != null) {
                __doPostBack(UpdatePanel1, '');
            }
        }
        function OnErrorCall(response) {
            document.getElementById('lblError').value = "Vartotojo vardas ir el paštas jau naudojami.";
            document.getElementById('txtPassword').value = "Slaptažodžiai nesutampa";
            
        }

        $.backstretch(
            [
                "img/4.jpg",
                "img/5.jpg",
                "img/6.jpg"

            ],

            {
                duration: 4500,
                fade: 1500
            }
        );
    </script>
</body>
</html>

