﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{/// <summary>
/// Class which holds methods for displaying seats
/// </summary>
    public partial class UserModules : System.Web.UI.Page
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CSSConnectionString"].ConnectionString);      //Philharmony database connection string

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.Request.IsAuthenticated)
            {
                Response.Redirect("/Login.aspx");
            }
            else
            {
                try
                {
                    hdnToken.Value = Session["Token"].ToString();
                    hdnUserID.Value = string.IsNullOrEmpty(Session["Userid"].ToString()) ? Session["Userid"].ToString() : CSSHelper.GetUserID().ToString();
                    BindGrid();
                   
                }
                catch (NullReferenceException ee)
                {
                    FormsAuthentication.SignOut();
                    Response.Redirect("/Login.aspx");
                }
            }
        }

        protected void BindGrid()
        {

            string url = "http://localhost:8080/api/Modules";
            string x = CSSHelper.CallWebMethod(url);
            List<Module> modules = Helper.AsObjectList<Module>(x);
            txtModuleCount.Text = modules.Count.ToString();
            DataGrid1.DataSource = modules;
            DataGrid1.DataBind();

        }
        

    }
}