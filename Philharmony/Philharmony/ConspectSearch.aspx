﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConspectSearch.aspx.cs" Inherits="Philharmony.ConspectSearch" %>
<%@ Register Src="BannerTop.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Šperinkis</title>
    <link href="favicon.ico" rel="Shortcut Icon" type="image/x-icon" />
    <!-- ============ Google fonts ============ -->
    <script src="scripts/jquery.responsivetable.min.js"></script> 
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="mystyle.css">

    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style>
        @media (min-width: 768px) {
        }

        .ajax__html_editor_extender_texteditor {
            background-color: white;
            color: black
        }

        TextBox1$HtmlEditorExtenderBehavior_ExtenderContainer {
            color: black;
        }

        #ajax__html_editor_extender_buttoncontainer {
            background: linear-gradient(to top right, #00ccff 0%, #006699 100%);
        }

        .containerClass .ajax__html_editor_extender_container {
            width: 100% !important; /*important is really important at here*/
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
          <header> <uc1:Header ID="WebUserControl1" runat="server" /></header>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div style="margin: 0px 1000px">
                    <asp:Image ID="Image1" ImageUrl="img/loader.gif" AlternateText="Processing" runat="server" Height="60px" Width="60px" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel UpdateMode="Always" ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="padding-left:2%">
                <table style="background: linear-gradient(to bottom, #45484d 0%,#000000 100%); border: black solid thin; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */ width:40%; height:auto">
                    <tr>
                        <td>Modulis</td>
                        <td> <asp:DropDownList ID="filter_Module"   runat="server" Style="color: black; width: 200px">
                            <asp:ListItem Text="-----Visi-----" Value="0"></asp:ListItem>
                             </asp:DropDownList></td>
                    </tr>
                     <tr>
                        <td>Paieškos raktas</td>
                        <td>  <asp:TextBox runat="server" ID="filter_keyword" Width="200" ForeColor="black" BackColor="white"></asp:TextBox></td>
                    </tr>
                     <tr>
                        <td>Ieškoti</td>
                        <td><asp:HiddenField runat="server" ID="conspectRate"/> 
                            <asp:HiddenField runat="server" ID="conspectCount2"></asp:HiddenField>   <asp:ImageButton Width="70" Height="50" ImageUrl="img/research.svg"   runat="server" ID="Search2" OnClick="SearchClick" Tooltip='Ieškoti'/></td>
                    </tr>
                    </table>
                <br />
                <table class="TableBorder" cellspacing="1" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td>
                            
                          
                            <asp:DataGrid ID="DataGrid1"  Style="width:auto; max-width:1000px;height:auto;max-height:500px; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ *//* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */ font-size: 16px; "  CellPadding="3"
                               AllowSorting="True" AutoGenerateColumns="False" GridLines="none" CellSpacing="1"
                                runat="server">
  
                                <Columns>
                                    <asp:TemplateColumn>
                                        <ItemStyle Width="10%"></ItemStyle>
                                      
                                        <ItemTemplate>
                                             <asp:ImageButton  Width="35" Height="35" ImageUrl="img/005-pencil.svg"   runat="server"  Visible='<%#Eval("Edit").ToString()=="1" %>'  OnClientClick='<%#"editConspectus("+Eval("ConspectusID").ToString()+")"%>' ID="editButton" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="40%">
                                        <HeaderTemplate>
                                            Konspekto pavadinimas
                                        </HeaderTemplate>
                                        <ItemStyle CssClass="TableStyleCell1" />
                                        <ItemTemplate>
                                            <asp:LinkButton  OnClientClick= '<%#"OnConspectusClick("+Eval("ConspectusID")+")" %>'  runat="server"   ID="lbl_ConspectID" Text='<%# Eval("ConspectusName") %>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>


                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <%# "Reitingas" %>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="lblRating2" runat="server" ToolTip='<%#" Balsavo : "+Eval("RatedCount") %>' Text='<%# Eval("Rating") %>'>
                                            </asp:TextBox>
                                        </ItemTemplate>

                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <%# "Kaina" %>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRating" runat="server" ToolTip='Taškų skaičius reikalingas peržiūrai' Text='<%# Eval("Price") %>'>
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <%# "Reitinguoti" %>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" Enabled='<%#Eval("UserID").ToString()!=Session["UserID"].ToString() %>' OnClientClick='<%#"Saveid("+Eval("ConspectusID")+")" %>' OnClick="_1_OnClick" id="linkbt" Text="1"></asp:LinkButton>
                                            <asp:LinkButton runat="server"  Enabled='<%#Eval("UserID").ToString()!=Session["UserID"].ToString() %>' OnClientClick='<%#"Saveid("+Eval("ConspectusID")+")" %>' OnClick="LinkButton1_OnClick" id="LinkButton1" Text="2"></asp:LinkButton>
                                            <asp:LinkButton runat="server" Enabled='<%#Eval("UserID").ToString()!=Session["UserID"].ToString() %>' OnClientClick='<%#"Saveid("+Eval("ConspectusID")+")" %>' OnClick="LinkButton2_OnClick" id="LinkButton2" Text="3"></asp:LinkButton>
                                            <asp:LinkButton runat="server"  Enabled='<%#Eval("UserID").ToString()!=Session["UserID"].ToString() %>' OnClientClick='<%#"Saveid("+Eval("ConspectusID")+")" %>' OnClick="LinkButton3_OnClick" id="LinkButton3" Text="4"></asp:LinkButton>
                                            <asp:LinkButton runat="server"  Enabled='<%#Eval("UserID").ToString()!=Session["UserID"].ToString() %>' OnClientClick='<%#"Saveid("+Eval("ConspectusID")+")" %>' OnClick="LinkButton4_OnClick" id="LinkButton4" Text="5"></asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Position="TopAndBottom" HorizontalAlign="Left" PageButtonCount="20"
                                    Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid></td>
                    </tr>
                </table>
                <br>

               
                <table id="add_Table" style="display: none"  width="100%"
                    border="0" runat="server">
                    <tr style="width:100%; height:auto;">
                        <td>
                            <table style="width:100%" >
                                <tr style="width:100%">
                                    <td style="width:5%" align="center" colspan="2"><%= "Konspekto redagavimas" %></td>
                                    <td style="width:95%"></td>
                                </tr>
                                <tr style="width:95%">
                                    <td  align="right" id="tdConspectusID" style="width:5%;" ><%= "Konspekto id :" %></td>
                                    <td style="width:95%">

                                        <asp:TextBox ID="ConspectusID" Enabled="false" Style=" background-color: #FEFDEE; font-size: 11px; border: solid 1px #74BAF3; color: black; width:20%; min-width:250px;" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr style="width:95%">
                                    <td align="right"><%= "Konspekto pavadinimas" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;</td>
                                    <td style="width:5%">
                                        <asp:HiddenField ID="HdnConspectusID" runat="server" />
                                        <asp:TextBox ID="add_ConspectusName" Style="background-color: #FEFDEE; font-size: 11px; border: solid 1px #74BAF3; color: black;width:20%; min-width:250px;" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr style="width:100%">
                                    <td style="width:5%" align="right"><%= "Kaina" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;</td>
                                    <td style="width:95%">
                                        <asp:TextBox ID="add_Price" Style="background-color: #FEFDEE; font-size: 11px; border: solid 1px #74BAF3; color: black;width:20%; min-width:250px;" runat="server" ></asp:TextBox>&nbsp;</td>
                                </tr>
                                <tr style="width:100%">
                                    <td  style="width:5%"align="right"><%= "Modulis" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;</td>
                                    <td style="width:95%">
                                        <asp:DropDownList ID="Add_Module" runat="server" Style="color: black; width:20%; min-width:250px" ></asp:DropDownList>&nbsp</td>
                                </tr>
                                <tr style="width:100%; ;">
                                    <td style="width:5%" align="right"><%= "Tekstas" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;</td>
                                    <td style="width:95%; padding-bottom:5%" >
                                        <div class="containerClass">
                                        <asp:TextBox runat="server" style="width:100%; height:auto; min-height:500px; max-width:1000px;"  ForeColor="black" ID="TextBox1"></asp:TextBox>
                                        <ajaxToolkit:HtmlEditorExtender OnImageUploadComplete="HtmlEditorExtender1_ImageUploadComplete"
                                            DisplaySourceTab="true" ID="HtmlEditorExtender1"
                                            TargetControlID="TextBox1"
                                            runat="server" >
                                        <toolbar> 
        <ajaxToolkit:Undo />
        <ajaxToolkit:Redo />
        <ajaxToolkit:Bold />
        <ajaxToolkit:Italic />
                                           
        <ajaxToolkit:Underline />
        <ajaxToolkit:StrikeThrough />
        <ajaxToolkit:Subscript />
        <ajaxToolkit:Superscript />
        <ajaxToolkit:JustifyLeft />
        <ajaxToolkit:JustifyCenter />
        <ajaxToolkit:JustifyRight />
        <ajaxToolkit:JustifyFull />
        <ajaxToolkit:InsertOrderedList />
        <ajaxToolkit:InsertUnorderedList />
        <ajaxToolkit:CreateLink />
        <ajaxToolkit:UnLink />
        <ajaxToolkit:RemoveFormat />
        <ajaxToolkit:SelectAll />
        <ajaxToolkit:UnSelect />
        <ajaxToolkit:Delete />
        <ajaxToolkit:Cut />
        <ajaxToolkit:Copy />
        <ajaxToolkit:Paste />
        <ajaxToolkit:BackgroundColorSelector />
        <ajaxToolkit:ForeColorSelector />
        <ajaxToolkit:FontNameSelector />
        <ajaxToolkit:FontSizeSelector />
        <ajaxToolkit:Indent />
        <ajaxToolkit:Outdent />
        <ajaxToolkit:InsertHorizontalRule />
        <ajaxToolkit:HorizontalSeparator />
         <ajaxToolkit:InsertImage />
</toolbar>
                                        </ajaxToolkit:HtmlEditorExtender>
                                            </div>
                                    </td>
                                </tr>
                    </tr>

                    <tr>
                        <td align="right"></td>
                        <td>
                            <asp:HiddenField ID="hdnUserID" Value='<%#Session["Userid"] %>' runat="server" />
                              <asp:ImageButton Width="70" Height="50" ImageUrl="img/icon.svg"   runat="server" OnClientClick='javascript:AddConspectus()' Tooltip='Išsaugoti'/>
                                                         <asp:ImageButton ID="Buttons"  Width="70" Height="50" ImageUrl="img/cancel.svg"
                                            OnClientClick="javascript:window.location.href='ConspectSearch.aspx'; return false;" runat="server"
                                            Tooltip='Atšaukti'></asp:ImageButton>
                            <asp:HiddenField ID="hdnToken" Value='<%#Session["Token"] %>' runat="server" />
                            <asp:HiddenField ID="conspectusidhdn"  runat="server" />
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
             <hr />
                    <footer>
                        <uc1:Footer ID="footer1" runat="server" />
                    </footer>
    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">

        $.ajaxSetup({
            beforeSend: function (xhr) {
                var h = document.getElementById('hdnToken');
                var token = h.value;
                xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            }
        });

        function OnConspectusClick(x) {
            window.open('http://localhost/conspectus.aspx?ConspectusID=' + x, 'newwin', 'height=400px,width=800px');

        }
        function editConspectus(x) {

            var h = document.getElementById('hdnToken');
            var token = h.value;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", "http://localhost:8080/api/Conspectus/" + x, false);
            xmlhttp.setRequestHeader('Authorization', 'Bearer ' + token);
            xmlhttp.send(null);
            var response = xmlhttp.responseText;
            var json = JSON.parse(response);
            var textbox = document.getElementById('HtmlEditorExtender1');
            var textbox2 = document.getElementById('add_ConspectusName');
            var textbox3 = document.getElementById('add_Price');
            var ConspectusID = document.getElementById('ConspectusID');
            var moduleidlbl = document.getElementById('tdConspectusID');
            var h2 = document.getElementById('add_Table');
            h2.style.display = "block";
            var e = document.getElementById("Add_Module");

            textbox2.value = json["ConspectusName"];
            textbox3.value = json["Price"];
            ConspectusID.value = json["ConspectusID"];
            e.options[e.selectedIndex].value = json["ModuleID"];
            var htmlEditorExtender = $('.ajax__html_editor_extender_texteditor');
            htmlEditorExtender.html(json["HtmlText"]);


        }

        function Saveid(x) {
            var conidhd = document.getElementById('conspectusidhdn');
            conidhd.value = x;
        }
        function AddConspectus() {
            var UserIDhdn = document.getElementById('hdnUserID');
            var textbox = document.getElementById('TextBox1');
            var textbox2 = document.getElementById('add_ConspectusName');
            var textbox3 = document.getElementById('add_Price');
            var ConspectusIDt = document.getElementById('ConspectusID');
            var e = document.getElementById("Add_Module");
            var moduleid = e.options[e.selectedIndex].value;
            var Name = textbox2.value;
            var html = textbox.value;
            var Price = textbox3.value;
            var ConspectusID = ConspectusIDt.value;
            var userid = UserIDhdn.value;
            var url = 'http://localhost:8080/api/Conspectus';
            if (ConspectusID) {
                var Conspectus = {
                    ConspectusID: ConspectusID,
                    Price: Price,
                    ConspectusName: Name,
                    ModuleID: moduleid,
                    HtmlText: html,
                    UserID: userid
                }

                $.ajax({
                    type: "PUT",
                    url: url + "/" + ConspectusID,
                    data: JSON.stringify(Conspectus),
                    contentType: "application/json; charset=utf-8",
                    success: onSuccess,
                    error: OnErrorCall
                });
            }
            else {
                var Conspectus = {
                    ConspectusName: Name,
                    ModuleID: moduleid,
                    Price: Price,
                    HtmlText: html,
                    UserID: userid
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify(Conspectus),
                    contentType: "application/json; charset=utf-8",
                    success: onSuccess,
                    error: OnErrorCall
                });
            }

            function onSuccess(response) {
                alert('Sėkmingai pridėta');
                var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';

                if (UpdatePanel1 != null) {
                    __doPostBack(UpdatePanel1, '');
                }
            }
            function OnErrorCall(response) {
                alert(response);
            }
        }

        $.backstretch(
            [
                "img/4.jpg",
                "img/5.jpg",
                "img/6.jpg"

            ],

            {
                duration: 4500,
                fade: 1500
            }
        );


    </script>
</body>
</html>
