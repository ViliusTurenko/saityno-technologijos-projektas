﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{
    public partial class conspectus : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CSSConnectionString"].ConnectionString);      //Philharmony database connection string
        protected void Page_Load(object sender, EventArgs e)
        {
             
            ConspectStatus.Value = CSSHelper.getConspect(Session["UserID"].ToString(), qConspectusID); ;
            htmldiv.InnerHtml = htmldiv.InnerHtml + CSSHelper.getConspect(Session["UserID"].ToString(), qConspectusID);
        }

       
        public string qConspectusID
        {
            get { return Request.QueryString["ConspectusID"]; }
        }

    }
}