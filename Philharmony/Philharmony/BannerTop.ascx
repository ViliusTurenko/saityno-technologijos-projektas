﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BannerTop.ascx.cs" Inherits="Philharmony.BannerTop1" %>

<style>
    @keyframes example {
    0%   {background-color:gray; left:0px; top:0px;}
    25%  {background-color:yellow; left:200px; top:0px;}
    50%  {background-color:blue; left:200px; top:200px;}
    75%  {background-color:green; left:0px; top:200px;}
    100% {background-color:white; left:0px; top:0px;}
}
  .img{ background:white; border-radius:15px;animation-name: example;
    animation-duration: 6s;}
</style>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <div id="custom-bootstrap-menu" style="font-family: 'Open Sans', sans-serif;" class="navbar navbar-default " role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Šperinkis.</a>
               <img class="img" src="img/ssss.jpg" style="width:100%;height:auto; max-width:150px;" />
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                        class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse navbar-menubuilder">
                <ul class="nav navbar-nav navbar-right">
   
                    <li><a href="/Main.aspx"><img border="0" alt="Pagrindinis" title="Pagrindinis" src="img/house.svg" width="50" height="50"><p>Pagrindinis</p></a> </li>

                    <li><a href="/UserModules.aspx"><img border="0" alt="Studentų moduliai" title="Studentų moduliai" src="img/report.svg" width="50" height="50"> <p>Studentų moduliai</p></a> </li>
                    <li><a href="/UserConspects.aspx"><img border="0" alt="Mano konspektai" title="Mano konspektai" src="img/008-notebook-1.svg" width="50" height="50"><p>Mano konspektai</p></a> </li>
                    <li><a href="/ConspectSearch.aspx"><img border="0" alt="Konspektų paieška" title="Konspektų paieška" src="img/004-search.svg" width="50" height="50"><p>Konspektų paieška</p></a> </li>
                    <li><a href="/UserInfo.aspx">
                        <li><a href="/UserInfo.aspx">
                            <asp:Label runat="server" Text="Prisijungęs kaip:" ID="loginNamelbl"></asp:Label>
                            <asp:LoginName ID="LoginName1" runat="server" Font-Bold="true" />
                        </a></li>
                        <li>
                                                 <asp:LoginStatus ID="LoginStatus1" LoginImageUrl="img/sign-in.svg" Width="50"  Height="50" LogoutImageUrl="img/login (1).svg" runat="server" />
                        </li>
                </ul>
            </div>
        </div>
    </div>

</body>
</html>
