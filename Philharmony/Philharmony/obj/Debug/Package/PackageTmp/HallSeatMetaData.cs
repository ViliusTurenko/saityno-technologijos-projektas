﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Philharmony
{
    [Serializable]
    [XmlRoot("HallSeat")]
    public class HallSeatMetaData
    {
        [XmlElement("ShowSeatID")]
        public int ShowSeatID { get; set; }
        [XmlElement("Color")]
        public string Color { get; set; }
        [XmlElement("Price")]
        public decimal Price { get; set; }
        [XmlElement("SeatRowLetter")]
        public string SeatRowLetter { get; set; }
        [XmlElement("SeatNumber")]
        public Nullable<int> SeatNumber { get; set; }
        [XmlElement("SeatNumberLetter")]
        public string SeatNumberLetter { get; set; }
        [XmlElement("IsReserved")]
        public string IsReserved { get; set; }
        [XmlElement("HallGroupID")]
        public int HallGroupID { get; set; }
        [XmlElement]
        public virtual HallGroup HallGroup { get; set; }
    }
    [MetadataType(typeof(HallSeatMetaData))]
    public partial class HallSeat
    { }
}