﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Philharmony.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Philharmony</title>
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style8 {
            width: 314px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 15px;
            padding-right: 15px;
            height: 190px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Philharmony</a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/Login.aspx">Login</a> </li>
                        <li><a href="/Registration.aspx">Register</a> </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                <div id="banner">
                    <h1>
                        <strong>Philharmony reservation system login</strong><span class="icon-bar"></span></h1>
                </div>
            </div>
        </div>
        <asp:Label ID="LabelEmpty" runat="server" Font-Size="24px" Text=""></asp:Label>
        <asp:UpdatePanel ID="up" runat="server">
            <ContentTemplate>
                <div class="auto-style8">
                    <asp:Login ID="Login1" runat="server" OnAuthenticate="ValidateUser" BackColor="Black" BorderColor="#B5C7DE" BorderPadding="4" BorderStyle="Double" BorderWidth="4px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="White" Height="230px" Width="397px">
                        <InstructionTextStyle Font-Italic="True" ForeColor="White" />
                        <LoginButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.9em" ForeColor="#284E98" />
                        <TextBoxStyle Font-Size="1.5em" ForeColor="Black" />
                        <TitleTextStyle BackColor="Black" Font-Bold="True" Font-Size="0.9em" ForeColor="White" />
                    </asp:Login>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">
        $.backstretch(
        [
            "img/6.jpg",
            "img/5.jpg",
            "img/4.jpg",
            "img/7.jpg",
            "img/9.jpg"
        ],

        {
            duration: 4500,
            fade: 1500
        }
    );
    </script>
</body>
</html>


