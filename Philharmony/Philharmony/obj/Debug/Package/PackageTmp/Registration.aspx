﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="Philharmony.Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Philharmony</title>
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style8 {
            width: 485px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .auto-style9 {
            width: 241px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Philharmony</a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/Login.aspx">Login</a> </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                <div id="banner">
                    <h1>
                        <strong>Philharmony seat registration</strong><span class="icon-bar"></span></h1>
                </div>
            </div>
        </div>
        <asp:UpdatePanel ID="up" runat="server">
            <ContentTemplate>
                <<div class="auto-style8" style="background-color: black;">
                    <table border="1" style="border-style: solid;">
                        <tr>
                            <th colspan="3">Registration
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="UsernameLabel" runat="server" Text="">Username</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox Style="background-color: black; border-style: solid; border-color: white white;" ID="txtUsername" runat="server" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtUsername"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="">Password</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox Style="background-color: black; border-style: solid; border-color: white white;" ID="txtPassword" runat="server" TextMode="Password" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtPassword"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="">Confirm password</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox Style="background-color: black; border-style: solid; border-color: white white;" ID="txtConfirmPassword" runat="server" TextMode="Password" />
                            </td>
                            <td class="auto-style9">
                                <asp:CompareValidator ErrorMessage="Passwords do not match." ForeColor="Red" ControlToCompare="txtPassword"
                                    ControlToValidate="txtConfirmPassword" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text="">Name</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox Style="background-color: black; border-style: solid; border-color: white white;" ID="Name" runat="server" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Required" ForeColor="Red" ControlToValidate="Name"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="">Last name</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox Style="background-color: black; border-style: solid; border-color: white white;" ID="Lastname" runat="server" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Required" ForeColor="Red" ControlToValidate="Lastname"
                                    runat="server" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="Label5" runat="server" Text="">Email</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox Style="background-color: black; border-style: solid; border-color: white white;" ID="txtEmail" runat="server" />
                            </td>
                            <td class="auto-style9">
                                <asp:RequiredFieldValidator ErrorMessage="Required" Display="Dynamic" ForeColor="Red"
                                    ControlToValidate="txtEmail" runat="server" />
                                <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ControlToValidate="txtEmail" ForeColor="Red" ErrorMessage="Invalid email address." />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button CssClass="btn btn-primary" ID="btnRegister" Text="Submit" runat="server" OnClick="RegisterUser" />
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="LabelEmpty" runat="server" Font-Size="24px" Text=""></asp:Label>
                    <br />
                    <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Login.aspx" runat="server"></asp:HyperLink>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRegister" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <div class="auto-style7" id="divv2">
        </div>
    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">
        $.backstretch(
        [
            "img/6.jpg",
            "img/5.jpg",
            "img/4.jpg",
            "img/7.jpg",
            "img/9.jpg"
        ],

        {
            duration: 4500,
            fade: 1500
        }
    );
    </script>
</body>
</html>

