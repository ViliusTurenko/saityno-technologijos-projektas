﻿/*Login.aspx.cs
2.v1.0
3.2017/06/07
4.2017/06/08
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{
    /// <summary>
    /// Class which holds method for user login
    /// </summary>
    public partial class Login : System.Web.UI.Page
    {
        public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["PhilharmonyConnectionString"].ConnectionString); //Philharmony database connection string
        /// <summary>
        /// Main method for Login page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [WebMethod(EnableSession = true)]
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
        }
        /// <summary>
        /// Method for user validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ValidateUser(object sender, EventArgs e)
        {
            int userId = 0;
            string constr = ConfigurationManager.ConnectionStrings["PhilharmonyConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("web_Validate_User"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Username", Login1.UserName);
                    cmd.Parameters.AddWithValue("@Password", Login1.Password);
                    cmd.Connection = con;
                    con.Open();
                    userId = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                switch (userId)                     //Check userId value
                {
                    case -1:
                        LabelEmpty.Text = "Username and/or password is incorrect.";      //If -1 username is incorrect
                        LabelEmpty.ForeColor = System.Drawing.Color.Red;
                        break;
                    case -2:
                        LabelEmpty.Text = "Account has not been activated.";            //If -2 accout has not been activated
                        break;
                    default:
                        #region get user type
                        con.Open();
                        SqlCommand cmd = new SqlCommand("[dbo].[web_User_Select]", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Username", Login1.UserName));
                        cmd.ExecuteNonQuery();
                        DataSet ds = new DataSet();
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                        DataRow Dr = dt.Rows[0];
                        Session["UserType"] = (string)Dr["UserType"];
                        con.Close();
                        #endregion
                        FormsAuthentication.RedirectFromLoginPage(Login1.UserName, Login1.RememberMeSet);
                        break;
                }
            }
        }
    }
}