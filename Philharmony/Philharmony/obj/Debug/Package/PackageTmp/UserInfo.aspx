﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserInfo.aspx.cs" Inherits="Philharmony.UserInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
    <!--[if IE 9]> <html class="no-js ie9 oldie" lang="en"> <![endif]-->
    <meta charset="utf-8" />
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Philharmony</title>
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style1 {
            width: 434px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .auto-style2 {
            width: 232px;
        }

        .auto-style3 {
            width: 436px;
            margin-left: 0px;
        }

        .auto-style4 {
            width: 101px;
        }
    </style>
</head>
<body>


    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Philharmony</a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/Reservation.aspx">Home</a> </li>
                        <%if ((string)Session["UserType"] == "Admin")
                            { %>
                        <li><a href="/Xml.aspx">Import data</a> </li>
                        <% }%>
                        <li><a href="/UserTickets.aspx">My tickets</a> </li>
                        <li><a href="/Seats.aspx">Available seats</a> </li>
                        <li><a href="/UserInfo.aspx">Logged in as:
                                    <asp:LoginName ID="LoginName1" runat="server" Font-Bold="true" />
                        </a></li>
                        <li>
                            <asp:LoginStatus ID="LoginStatus1" runat="server" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="up">
            <ProgressTemplate>
                <div style="margin-left: 160px">
                    <asp:Image ID="Image1" ImageUrl="img/loader.gif" AlternateText="Processing" runat="server" />
                </div>

            </ProgressTemplate>
        </asp:UpdateProgress>

        <asp:UpdatePanel ID="up" runat="server">
            <ContentTemplate>
                <div class="auto-style1">
                    <table style="background-color: black; border: double; border-color: white" class="auto-style3">
                        <tr>
                            <th colspan="3">
                                <asp:Label ID="Label2" runat="server" Text="">Account details for user:</asp:Label>
                            </th>
                            <td>
                                <asp:TextBox Style="background-color: black;" ID="txtUserName" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style4">
                                <asp:Label ID="Label3" runat="server" Text="">First name</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox Style="background-color: black; border-style: solid; border-color: white white;" ID="txtName0" runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:RequiredFieldValidator ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtName0"
                                    runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:TextBox ID="txtName" runat="server" Style="background-color: black; border-style: solid; border-color: white white;" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style4">
                                <asp:Label ID="Label4" runat="server" Text="">Last name</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox Style="background-color: black; border-style: solid; border-color: white white;" ID="txtLastname0" runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:RequiredFieldValidator ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtLastname0"
                                    runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:TextBox ID="txtLastname" runat="server" ReadOnly="true" Style="background-color: black; border-style: solid; border-color: white white;" />
                            </td>
                        </tr>

                        <tr>
                            <td class="auto-style4">
                                <asp:Label ID="Label5" runat="server" Text="">Personal Email</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox Style="background-color: black; border-style: solid; border-color: white white;" ID="txtEmail0" runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:RequiredFieldValidator ErrorMessage="Required" Display="Dynamic" ForeColor="Red"
                                    ControlToValidate="txtEmail0" runat="server" />
                                <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ControlToValidate="txtEmail0" ForeColor="Red" ErrorMessage="Invalid email address." />
                            </td>
                            <td class="auto-style2">
                                <asp:TextBox ID="txtEmail" runat="server" Style="background-color: black; border-style: solid; border-color: white white;" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style4">
                                <asp:Label ID="Label1" runat="server" Text="">Password</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox Style="background-color: black; border-style: solid; border-color: white white;" ID="txtPassword0" TextMode="Password" runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:RequiredFieldValidator ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtPassword0"
                                    runat="server" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label6" runat="server" Text="">Confirm password</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox Style="background-color: black; border-style: solid; border-color: white white;" ID="txtConfirmPassword" runat="server" TextMode="Password" />
                            </td>
                            <td class="auto-style9">
                                <asp:CompareValidator ErrorMessage="Passwords do not match." ForeColor="Red" ControlToCompare="txtPassword0"
                                    ControlToValidate="txtConfirmPassword" runat="server" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="auto-style4"></td>
                            <td>
                                <asp:Button CssClass="btn btn-primary" ID="btnUpdate" Text="Save changes" runat="server" OnClick="UpdateUser" />
                            </td>
                            <td class="auto-style2"></td>
                            <td class="auto-style2"></td>

                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">

        $.backstretch(
        [
            "img/9.jpg",
            "img/5.jpg",
            "img/6.jpg",
            "img/7.jpg",
            "img/4.jpg"
        ],

        {
            duration: 4500,
            fade: 1500
        }
    );

    </script>
</body>
</html>
