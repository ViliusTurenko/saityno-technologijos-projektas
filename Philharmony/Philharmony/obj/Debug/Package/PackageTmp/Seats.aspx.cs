﻿/*Seats.aspx.cs
2.v1.0
3.2017/05/08
4.2017/06/13
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{/// <summary>
/// Class which holds methods for displaying seats
/// </summary>
    public partial class Seats : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["PhilharmonyConnectionString"].ConnectionString);      //Philharmony database connection string
        #region variables
        public int[] RowsParter = { 21, 22, 21, 22, 21, 22, 21, 22, 21, 22, 21, 22, 21, 21, 20, 18, 18, 18, 18, 18, 16, 14, 14, 16 };       //Rows and number of seats in parter
        public int[] RowsBalcony = { 20, 20, 20, 20, 20, 20, 20, 20, 20 };                                                                  //Rows and number of seats in center of balcony
        public int[] RowsParterSmallHall = { 21, 22, 21, 22, 21, 22, 21, 22, 21, 22, 21 };                                                   //Rows and number of seats in small hall parter
        public int[] RowsLongParter = { 21, 22, 21, 22, 21, 22, 21 };                                                                        //Rows and number of seats in long parter
        public int BalconyLeft = 15;                                                                                                        //Number of seats in left balcony(only one row exists)
        public int BalconyRight = 15;                                                                                                       //Number of seats in right balcony(only one row exists)
        public string seat_no = "";                                                                                                         //Seat number string
        public string getSeat;                                                                                                              //Reserved seat variable
        public string chair;                                                                                                                //Chair variable used in layout
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)                                                                                   //If user is not logged in
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                if (ddlEvent.Items.Count == 0)                                                                           //If ddlHall is empty
                {
                    #region populate ddlEvent
                    con.Open();
                    SqlCommand cmd5 = new SqlCommand("Web_Event_select", con);                                           //execute stored procedure web_Event_Select with parameters
                    cmd5.CommandType = CommandType.StoredProcedure;
                    cmd5.ExecuteNonQuery();
                    DataTable dt5 = new DataTable();
                    SqlDataAdapter da5 = new SqlDataAdapter(cmd5);
                    da5.Fill(dt5);
                    ddlEvent.DataSource = dt5;
                    ddlEvent.DataTextField = "Name";
                    ddlEvent.DataValueField = "Name";
                    ddlEvent.DataBind();
                    ddlEvent.Items.Insert(0, new ListItem(String.Empty, String.Empty));                                  //Insert new empty row
                    con.Close();
                    #endregion
                }
            }
        }

        /// <summary>
        /// Method for checking if seat is occupied
        /// </summary>
        /// <param name="sitting">seat information(row and seat number)</param>
        /// <returns></returns>
        protected string checkSeat(string sitting)
        {

            try
            {
                string[] words = getSeat.Split(' ');            //Split all seats in to array
                foreach (string word in words)
                {

                    try
                    {
                        if (sitting.Equals(word))                               //If seat is occupied
                        {
                            chair = "<img id='" + sitting + "' alt='Booked' src='img/chair/R_chair.gif'/>";
                            return chair;
                        }
                        else                                                    //If seat is not occupied
                        {
                            string hallgroup = "";
                            string rowletter = "";
                            #region Get hall group id
                            if (sitting.ToLower().Contains("a"))
                            {
                                rowletter = "A";
                            }
                            if (sitting.ToLower().Contains("ls"))
                            {
                                hallgroup = "39";
                            }
                            if (sitting.ToLower().Contains("sr"))
                            {
                                hallgroup = "40";
                            }
                            if (sitting.ToLower().Contains("p"))
                            {
                                hallgroup = "41";
                            }
                            if (sitting.ToLower().Contains("bl"))
                            {
                                hallgroup = "43";
                            }
                            if (sitting.ToLower().Contains("br"))
                            {
                                hallgroup = "42";
                            }
                            if (sitting.ToLower().Contains("bc"))
                            {
                                hallgroup = "44";
                            }
                            #endregion
                            string x = Regex.Replace(sitting, "[A-Za-z ]", "");//Remove alphabetical characters from seat data 
                            x = x.Replace(":", " ");                           //Remove : character
                            x = x + " " + hallgroup + " " + rowletter + " " + sitting;  //Add additional data 
                            chair = "<img id='" + sitting + "' alt='Unbooked'   onmouseover = \"mouseOver('" + x + "')\" onmouseout = \"mouseOut('" + x + "')\" onclick=\"jsReserve('" + x + "')\" title='Unbooked,' src='img/chair/G_chair.gif' style='cursor:pointer;'/>";
                        }
                    }
                    catch (ArgumentOutOfRangeException) { }
                }
            }
            catch (NullReferenceException)
            {
                string hallgroup = "";
                string rowletter = "";
                #region Get hall group id
                if (sitting.ToLower().Contains("a"))
                {
                    rowletter = "A";
                }
                if (sitting.ToLower().Contains("sr"))
                {
                    hallgroup = "40";
                }
                if (sitting.ToLower().Contains("p"))
                {
                    hallgroup = "41";
                }
                if (sitting.ToLower().Contains("bl"))
                {
                    hallgroup = "43";
                }
                if (sitting.ToLower().Contains("br"))
                {
                    hallgroup = "42";
                }
                if (sitting.ToLower().Contains("ls"))
                {
                    hallgroup = "39";
                }
                if (sitting.ToLower().Contains("bc"))
                {
                    hallgroup = "44";
                }
                #endregion
                string x = Regex.Replace(sitting, "[A-Za-z ]", "");//Remove alphabetical characters from seat data 
                x = x.Replace(":", " ");                           //Remove : character
                x = x + " " + hallgroup + " " + rowletter + " " + sitting;  //Add additional data 
                chair = "<img id='" + sitting + "' alt='Unbooked'   onmouseover = \"mouseOver('" + x + "')\" onmouseout = \"mouseOut('" + x + "')\" onclick=\"jsReserve('" + x + "')\" title='Unbooked,' src='img/chair/G_chair.gif' style='cursor:pointer;'/>";
            }
            return chair;
        }
        /// <summary>
        /// Method for finding occupied seats in database and updating seat reservation status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void generateSeat(object sender, EventArgs e)
        {
            con.Open();
            var HallID = ddlHall.SelectedValue;                         //Get selected HallId value from ddlHall
            Session["HallID"] = HallID;                                 //Save HallID to session
            #region get selected event id
            var EventName = (string)(ddlEvent.SelectedValue);           //Get Event name from ddlEvent
            SqlCommand cmd6 = new SqlCommand("web_Event_SelectEventID", con);//Stored procedure to count reserved seats 
            cmd6.CommandType = CommandType.StoredProcedure;
            cmd6.Parameters.Add(new SqlParameter("@EventName", EventName));
            cmd6.Parameters.Add(new SqlParameter("@HallID", HallID));
            cmd6.Parameters.Add("@Event", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd6.ExecuteNonQuery();
            var EventID = int.Parse(cmd6.Parameters["@Event"].Value.ToString());         //Get selected event id
            #endregion
            #region count occupied and free seats
            Session["EventID"] = EventID;                               //Save EventID to session
            if (!string.IsNullOrEmpty(Session["EventID"].ToString()))      //If EventID is valid
            {

                SqlCommand cmd1 = new SqlCommand("web_HallSeatReserved_select", con);           //execute stored procedure web_Hall_Select with parameters
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.Add(new SqlParameter("@EventID", EventID));
                cmd1.Parameters.Add(new SqlParameter("@HallID", HallID));
                cmd1.ExecuteNonQuery();
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                da.Fill(dt);
                for (int i = 0; i < dt.Rows.Count; i++)                                 //Put reserved seats data to one string
                {
                    getSeat = getSeat + dt.Rows[i][0].ToString().Trim() + dt.Rows[i][2].ToString().Trim() + dt.Rows[i][3].ToString().Trim() + ":" + dt.Rows[i][1].ToString().Trim() + " ";
                }
            }
            con.Close();
            #endregion
        }
        /// <summary>
        /// Method to handle ddlHall selected value change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region populate Hall drop down list
            con.Open();
            string EventName = "";
            if (!string.IsNullOrEmpty(ddlEvent.SelectedValue))
            {
                EventName = (string)(ddlEvent.SelectedValue);//Get HallName from ddlHall
            }
            SqlCommand cmd4 = new SqlCommand("web_Hall_Select", con);                                                       //execute stored procedure web_Event_Select with parameters
            cmd4.Parameters.AddWithValue("@Name", EventName);
            cmd4.CommandType = CommandType.StoredProcedure;
            cmd4.ExecuteNonQuery();
            DataTable dt4 = new DataTable();
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            da4.Fill(dt4);
            ddlHall.DataSource = dt4;
            ddlHall.DataTextField = "Name";
            ddlHall.DataValueField = "HallID";
            ddlHall.DataBind();
            ddlHall.Items.Insert(0, new ListItem(String.Empty, String.Empty));                                              //Insert first empty row to ddlEvent
            con.Close();
            #endregion
        }
    }
}