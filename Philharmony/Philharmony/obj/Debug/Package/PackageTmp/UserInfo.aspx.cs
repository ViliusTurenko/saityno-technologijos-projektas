﻿/*UserInfo.aspx.cs
2.v1.0
3.2017/06/07
4.2017/06/08
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace Philharmony
{
    /// <summary>
    /// Class for holding methods which display user info
    /// </summary>
    public partial class UserInfo : System.Web.UI.Page
    {
        public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["PhilharmonyConnectionString"].ConnectionString);//Philharmony database connection string
        /// <summary>
        /// Main method for User info page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                #region if Session["userId"] is null
                if (Session["UserId"] == null)                                          //If user id is not saved to session
                {
                    using (SqlCommand cmd1 = new SqlCommand("[dbo].[web_User_SelectID]"))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter())
                        {
                            cmd1.CommandType = CommandType.StoredProcedure;
                            cmd1.Parameters.AddWithValue("@Username", User.Identity.Name);
                            cmd1.Connection = con;
                            con.Open();
                            int Id = Convert.ToInt32(cmd1.ExecuteScalar());            //Get user id
                            Session["UserId"] = Id;                                    //Save user id
                            con.Close();
                        }
                    }
                }
                #endregion
                #region get user data
                con.Open();
                SqlCommand cmd = new SqlCommand("[dbo].[web_User_Select]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Username", User.Identity.Name));
                cmd.ExecuteNonQuery();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                DataRow Dr = dt.Rows[0];
                txtEmail.Text = (string)Dr["Email"];
                txtName.Text = (string)Dr["Name"];
                txtLastname.Text = (string)Dr["Lastname"];
                txtUserName.Text = User.Identity.Name;
                con.Close();
                #endregion
            }
        }
        /// <summary>
        /// Method for updating user information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateUser(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("[dbo].[web_User_update]", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Username", User.Identity.Name));
            cmd.Parameters.Add(new SqlParameter("@Name", txtName0.Text));
            cmd.Parameters.Add(new SqlParameter("@Lastname", txtLastname0.Text));
            cmd.Parameters.Add(new SqlParameter("@Password", txtPassword0.Text));
            cmd.Parameters.Add(new SqlParameter("@Email", txtEmail0.Text));
            int x = cmd.ExecuteNonQuery();
            con.Close();
            if (x == 1)         ///If update succesful, update displayed info fields
            {
                txtEmail.Text = txtEmail0.Text;
                txtName.Text = txtName0.Text;
                txtLastname.Text = txtLastname0.Text;
            }
        }
    }
}