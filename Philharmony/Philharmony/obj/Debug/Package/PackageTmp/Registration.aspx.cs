﻿/*Registration.aspx.cs
2.v1.0
3.2017/06/08
4.2017/06/13
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{
    /// <summary>
    /// Class which holds methods for registration
    /// </summary>
    public partial class Registration : System.Web.UI.Page
    {
        /// <summary>
        /// Main registration page method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Method for registering user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegisterUser(object sender, EventArgs e)
        {
            int userId = 0;
            string constr = ConfigurationManager.ConnectionStrings["PhilharmonyConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[web_User_Insert]"))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Username", txtUsername.Text.Trim());
                        cmd.Parameters.AddWithValue("@Name", Name.Text.Trim());
                        cmd.Parameters.AddWithValue("@Lastname", Lastname.Text.Trim());
                        cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                        cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                        cmd.Connection = con;
                        con.Open();
                        userId = Convert.ToInt32(cmd.ExecuteScalar());
                        con.Close();
                    }
                }
                string message = string.Empty;
                switch (userId)         //Check result of executed stored procedure
                {
                    case -1:
                        LabelEmpty.Text = "Username taken,please choose a different username.";  //If -1 then username taken
                        LabelEmpty.ForeColor = System.Drawing.Color.Red;
                        break;
                    case -2:
                        LabelEmpty.Text = "Supplied email address has already been used.";       //If -2 then email has already been used
                        LabelEmpty.ForeColor = System.Drawing.Color.Red;
                        break;
                    default:
                        LabelEmpty.Text = "Registration successful.";                            //Else success
                        LabelEmpty.ForeColor = System.Drawing.Color.Green;
                        HyperLink1.Text = "Press to login";
                        break;
                }
            }
        }
    }
}