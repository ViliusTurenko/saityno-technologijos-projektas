﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Seats.aspx.cs" Inherits="Philharmony.Seats" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Philharmony</title>
    <link href="favicon.ico" rel="Shortcut Icon" type="image/x-icon" />
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        img {
            width: 28px;
            height: 30px;
            margin-bottom: 15px;
            cursor: none;
            vertical-align: text-bottom;
        }

        .p_seat {
            color: white;
            font-size: 12px;
            font-weight: bold;
            margin-left: -40px;
            padding: 14px 14px 0px 0px;
        }

        .p_seat2 {
            color: white;
            font-size: 12px;
            font-weight: bold;
            margin-left: -40px;
            padding: 14px 14px 0px 10px;
        }

        .auto-style9 {
            width: 1030px;
            height: 1200px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .divl {
            -webkit-transform: rotate(270deg); /* Safari and Chrome */
            -moz-transform: rotate(270deg); /* Firefox */
            -ms-transform: rotate(270deg); /* IE 9 */
            -o-transform: rotate(270deg); /* Opera */
            transform: rotate(270deg);
        }

        .divr {
            -webkit-transform: rotate(90deg); /* Safari and Chrome */
            -moz-transform: rotate(90deg); /* Firefox */
            -ms-transform: rotate(90deg); /* IE 9 */
            -o-transform: rotate(90deg); /* Opera */
            transform: rotate(90deg);
        }

        .auto-style10 {
            width: 45px;
            height: 752px;
        }

        .auto-style11 {
            width: 60px;
            height: 1545px;
        }

        .auto-style12 {
            width: 1100px;
        }

        .auto-style13 {
            width: 76px;
        }

        .auto-style14 {
            width: 1070px;
            height: 458px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .auto-style15 {
            width: 45px;
            height: 744px;
        }

        .auto-style16 {
            -webkit-transform: rotate(270deg);
            -moz-transform: rotate(270deg);
            -ms-transform: rotate(270deg);
            -o-transform: rotate(270deg);
            transform: rotate(270deg);
            width: 165px;
        }

        .auto-style17 {
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);
            width: 134px;
            height: 18px;
        }

        .auto-style19 {
            background: #333;
            background: rgba(51, 51, 51, 0.2);
            color: #FFFFFF;
        }

        .auto-style20 {
            width: 45px;
        }

        .auto-style21 {
            width: 1231px;
            height: 626px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .auto-style22 {
            width: 887px;
            height: 370px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 15px;
            padding-right: 15px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Philharmony</a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/Reservation.aspx">Home</a> </li>
                        <%if ((string)Session["UserType"] == "Admin")
                            { %>
                        <li><a href="/Xml.aspx">Import data</a> </li>
                        <% }%>
                        <li><a href="/UserTickets.aspx">My tickets</a> </li>
                        <li><a href="/Seats.aspx">Available seats</a> </li>
                        <li><a href="/UserInfo.aspx">Logged in as:
                                    <asp:LoginName ID="LoginName1" runat="server" Font-Bold="true" />
                        </a></li>
                        <li>
                            <asp:LoginStatus ID="LoginStatus1" runat="server" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
            <ProgressTemplate>
                <div style="margin: 0px 1000px">
                    <asp:Image ID="Image1" ImageUrl="img/loader.gif" AlternateText="Processing" runat="server" Height="60px" Width="60px" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="container">
                    <table id="tblSeats" class="auto-style11">
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Select event<br />
                                <asp:DropDownList ID="ddlEvent" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" CssClass="auto-style19" runat="server" Height="32px" Width="265px" Style="margin-bottom: 0" AutoPostBack="true">
                                </asp:DropDownList>
                                <br />
                                Select hall<br />
                                <asp:DropDownList ID="ddlHall" OnSelectedIndexChanged="generateSeat" CssClass="auto-style19" runat="server" Height="32px" Width="265px" Style="margin-bottom: 0" AutoPostBack="true">
                                </asp:DropDownList><br />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><%if (!String.IsNullOrEmpty((string)ddlEvent.SelectedValue))
                                    {

                                        if (!string.IsNullOrEmpty(ddlHall.SelectedValue))
                                        {
                                            if ((ddlHall.SelectedValue) == "66") //If selected hall is Didzioji sale
                                            { %>
                                <div style="font-size: 20px; text-align: center;">
                                    Balcony center
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <div class="auto-style14" style="background-color: black; border: 1px solid white;">
                                    <%   
                                        int rowBalconyIndex = RowsBalcony.Length - 1;           //Get last row index
                                        int ii = RowsBalcony[rowBalconyIndex];
                                        string rowBalcony = "", seat_noBalcony = "";
                                        while (rowBalconyIndex >= 0)                            //While there are rows(getting them backwards)
                                        {
                                            while (ii >= 1)
                                            {
                                                if (ii >= 1)
                                                {
                                                    rowBalcony = (rowBalconyIndex + 15).ToString(); //Add 15 because balcony center rows start from 15
                                                }
                                                if (ii <= 9)
                                                    seat_noBalcony = "BC" + rowBalcony + ":" + ii;
                                                else
                                                    seat_noBalcony = "BC" + rowBalcony + ":" + ii;
                                                string src = checkSeat(seat_noBalcony);
                                    %>
                                    <%=chair %><span class="p_seat"> <%=seat_noBalcony %>   </span>
                                    <%if (ii == 1)
                                        { %>
                                    <br />
                                    <%} %>
                                    <%     ii--;
                                            }
                                            rowBalconyIndex--;
                                            if (rowBalconyIndex >= 0)
                                                ii = RowsBalcony[rowBalconyIndex];
                                        }
                                    %>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <div style="font-size: 20px; text-align: center;">
                                    Parter
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="auto-style16" style="font-size: 20px;">
                                    Balcony left
                                </div>
                            </td>
                            <td class="auto-style13">
                                <div id="first_column" style="background-color: black; border: 1px solid white;" class="auto-style15">
                                    <%   
                                        int j = BalconyLeft;
                                        string seat_no2 = "";
                                        while (j >= 1)
                                        {
                                            if (j <= 9)
                                                seat_no2 = "BL1:" + j;
                                            else
                                                seat_no2 = "BL1:" + j.ToString();
                                            string src = checkSeat(seat_no2);
                                    %>
                                    <%=chair %><span class="p_seat2"> <%=seat_no2 %>   </span>
                                    <%if (j == 1)
                                        { %>

                                    <br />

                                    <%} %>

                                    <%     j--;
                                        }

                                    %>
                                </div>
                            </td>
                            <td class="auto-style12">
                                <div class="auto-style9" style="background-color: black; border: 1px solid white;">
                                    <br />
                                    <%   
                                        int rowIndex = RowsParter.Length - 1;
                                        int i = RowsParter[rowIndex];
                                        string row = "", seat_no = "";
                                        while (rowIndex >= 0)
                                        {
                                            while (i >= 1)
                                            {
                                                if (i >= 1)
                                                {
                                                    if (rowIndex < 16)
                                                        row = (rowIndex + 1).ToString();
                                                    if (rowIndex > 16)
                                                        row = (rowIndex).ToString();
                                                    else if (rowIndex == 16)
                                                        row = (rowIndex).ToString() + "A";
                                                }
                                                if (i <= 9)
                                                    seat_no = "P" + row + ":" + i;
                                                else
                                                    seat_no = "P" + row + ":" + i;
                                                string src = checkSeat(seat_no);
                                    %>

                                    <%=chair %><span class="p_seat"> <%=seat_no %>   </span>
                                    <%if (i == 1)
                                        { %>

                                    <br />

                                    <%} %>

                                    <%     i--;
                                            }

                                            rowIndex--;
                                            if (rowIndex >= 0)
                                                i = RowsParter[rowIndex];

                                        }
                                    %>
                                </div>
                            </td>
                            <td class="auto-style20">
                                <div id="Right_column" style="background-color: black; border: 1px solid white;" class="auto-style10">

                                    <%   
                                        int k = BalconyRight;
                                        string seat_no3 = "";
                                        while (k >= 1)
                                        {
                                            if (j <= 9)
                                                seat_no3 = "BR1:" + k;
                                            else
                                                seat_no3 = "BR1:" + k.ToString();
                                            string src = checkSeat(seat_no3);
                                    %>
                                    <%=chair %><span class="p_seat2"> <%=seat_no3 %>   </span>
                                    <%if (k == 1)
                                        { %>

                                    <br />

                                    <%} %>

                                    <%     k--;
                                        }

                                    %>
                                </div>
                            </td>

                            <td>
                                <div class="auto-style17" style="font-size: 20px; text-align: center;">
                                    Balcony right
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <div style="font-size: 50px; text-align: center; background-color: black;">
                                    Scene
                                </div>
                            </td>
                            <td class="auto-style20"></td>
                            <td></td>
                        </tr>
                    </table>
                    <%}
                        else if ((ddlHall.SelectedValue) == "67")// if selected hall is LnfMazoji sale
                        { %>
                    <table>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <div style="font-size: 20px; text-align: center;">
                                    <asp:Label runat="server">Parter</asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="auto-style13"></td>
                            <td class="auto-style12">
                                <div class="auto-style21" style="background-color: black; border: 1px solid white;">
                                    <br />

                                    <%   

                                        int rowIndex = RowsParterSmallHall.Length - 1;
                                        int i = RowsParterSmallHall[rowIndex];
                                        string row = "", seat_no = "";
                                        while (rowIndex >= 0)
                                        {
                                            while (i >= 1)
                                            {
                                                if (i >= 1)
                                                {
                                                    row = (rowIndex + 1).ToString();
                                                }
                                                if (i <= 9)
                                                    seat_no = "SR" + row + ":" + i;
                                                else
                                                    seat_no = "SR" + row + ":" + i;
                                                string src = checkSeat(seat_no);
                                    %>

                                    <%=chair %><span class="p_seat"> <%=seat_no %>   </span>
                                    <%if (i == 1)
                                        { %>

                                    <br />

                                    <%} %>

                                    <%     i--;
                                            }

                                            rowIndex--;
                                            if (rowIndex >= 0)
                                                i = RowsParterSmallHall[rowIndex];

                                        }
                                    %>
                                </div>
                            </td>
                            <td class="auto-style20"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <div style="font-size: 50px; text-align: center; background-color: black;">
                                    Scene
                                </div>
                            </td>
                            <td class="auto-style20"></td>
                            <td></td>
                        </tr>
                    </table>
                    <%}
                        else if ((ddlHall.SelectedValue) == "64")//If selected hall is Lnf ilgoji sale
                        { %>
                    <table>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <div style="font-size: 20px; text-align: center;">
                                    <asp:Label runat="server">Long parter</asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="auto-style13"></td>
                            <td class="auto-style12">
                                <div class="auto-style22" style="background-color: black; border: 1px solid white;">
                                    <br />
                                    <%   
                                        int rowIndex = RowsLongParter.Length - 1;
                                        int i = RowsLongParter[rowIndex];
                                        string row = "", seat_no = "";
                                        while (rowIndex >= 0)
                                        {
                                            while (i >= 1)
                                            {
                                                if (i >= 1)
                                                {
                                                    row = (rowIndex + 1).ToString();
                                                }
                                                if (i <= 9)
                                                    seat_no = "LS" + row + ":" + i;
                                                else
                                                    seat_no = "LS" + row + ":" + i;
                                                string src = checkSeat(seat_no);
                                    %><%=chair %><span class="p_seat"><%=seat_no %></span><%if (i == 1)
                                                                                              { %>
                                    <br />
                                    <%} %><%     i--;
                                                  }

                                                  rowIndex--;
                                                  if (rowIndex >= 0)
                                                      i = RowsLongParter[rowIndex];
                                              }
                                    %>
                                </div>
                            </td>
                            <td class="auto-style20"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <div style="font-size: 50px; text-align: center; background-color: black;">
                                    Scene
                                </div>
                            </td>
                            <td class="auto-style20"></td>
                            <td></td>
                        </tr>
                    </table>
                    <%
                                }
                            }
                        }
                    %>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlEvent" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>


    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">
        $.backstretch(
        [
            "img/6.jpg",
            "img/4.jpg",
            "img/7.jpg",

        ],

        {
            duration: 4500,
            fade: 1500
        }
    );

        function seat(s) {
            document.getElementById("" + s + "").src = "img/chair/Gy_chair.gif";
        }
        //Function for handling mouse over image events
        function mouseOver(s) {
            var DdlEvent = document.getElementById("<%=ddlEvent.ClientID%>");
            var EventID = DdlEvent.options[DdlEvent.selectedIndex].value;
            var rn = s.split(' ')[0];
            var pn = s.split(' ')[1];
            var hgid = s.split(' ')[2];
            var rl = s.split(' ')[3];
            var img = s.split(' ')[4];
            var x = document.getElementById("" + img + "").title.split(',')[0];
            if (x == "Unbooked") {
                document.getElementById("" + img + "").src = "img/chair/Gy_chair.gif";
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET", "Reservation.aspx?opr=check&rn=" + rn + "&pn=" + pn + "&hgid=" + hgid + "&rl=" + rl + "&ei=" + EventID, false);
                xmlhttp.send(null);
                var response = xmlhttp.responseText;
                var status = response.split(',')[0];
                var price = response.split(',')[4];
                document.getElementById("" + img + "").title = "Unbooked,Price: " + price + "€";
            }

        }
        //Function for handling mouseOut event
        function mouseOut(s) {
            var img = s.split(' ')[4];
            var x = document.getElementById("" + img + "").title.split(',')[0];
            if (x == "Unbooked") {
                document.getElementById("" + img + "").src = "img/chair/G_chair.gif";
            }
            else if (x == "Booked") {
                document.getElementById("" + img + "").src = "img/chair/R_chair.gif";
            }
        }
        //Function for reserving seat
        function jsReserve(s) {
            var DdlEvent = document.getElementById("<%=ddlEvent.ClientID%>");
            var EventID = DdlEvent.options[DdlEvent.selectedIndex].value;
            var rn = s.split(' ')[0];
            var pn = s.split(' ')[1];
            var hgid = s.split(' ')[2];
            var rl = s.split(' ')[3];
            var img = s.split(' ')[4];
            document.getElementById("" + img + "").src = "img/chair/Gy_chair.gif";
            document.getElementById("" + img + "").title = "Booked,";
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", "Reservation.aspx?opr=reserve&rn=" + rn + "&pn=" + pn + "&hgid=" + hgid + "&rl=" + rl + "&ei=" + EventID, false);
            xmlhttp.send(null);
            var response = xmlhttp.responseText;
            if (response == "Success") {
                document.getElementById("" + img + "").src = "img/chair/R_chair.gif";
            }
            if (response == "Failed") {
                document.getElementById("" + img + "").src = "img/chair/Gy_chair.gif";
            }

        }
    </script>
</body>
</html>
