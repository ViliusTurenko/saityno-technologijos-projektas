﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reservation.aspx.cs" Inherits="Philharmony.Reservation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
    <!--[if IE 9]> <html class="no-js ie9 oldie" lang="en"> <![endif]-->
    <meta charset="utf-8">
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Philharmony</title>
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        div1 {
            background-color: lightblue;
        }

        .auto-style7 {
            width: 275px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .auto-style10 {
            width: 274px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 15px;
            padding-right: 15px;
        }
    </style>
</head>
<body>
  
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                 
                <div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">Philharmony</a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                                    class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse navbar-menubuilder">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="/">Home</a> </li>
                                <li><a href="/Seats.aspx">Seat layout</a> </li>
                                <li><a href="/Xml.aspx">Import data</a> </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                        <div id="banner">
                            <h1>
                                <strong>Philharmony seat reservation</strong><span class="icon-bar"></span></h1>
                        </div>
                    </div>
                </div>
                <div class="auto-style7" style="background-color: black;">


                    <asp:Label ID="RowNumberLabel" runat="server" Text="">Row number</asp:Label><br />
                    <asp:TextBox ID="RowNumber" Style="background-color: black; border-style: solid; border-color: white white;" runat="server"></asp:TextBox><br />
                    <asp:Label ID="RowNumberLetterLabel" runat="server" Text="">Row number letter</asp:Label>
                    <br />
                    <asp:DropDownList ID="RowLetter" CssClass="ddl" runat="server" Height="26px" Width="25px" Style="margin-bottom: 0"> 
                        <asp:ListItem Text="  -  " Value=""></asp:ListItem>
                        <asp:ListItem Text="  A  " Value="A"></asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <asp:Label ID="SeatNumberLabel" runat="server" Text="">Seat number</asp:Label>
                    <br />
                    <asp:TextBox ID="PlaceNumber" Style="background-color: black; border-style: solid; border-color: white white;" runat="server"></asp:TextBox>
                    <br />
                    <asp:Label ID="HallGroupDdlLabel" runat="server" Text="">Hall group</asp:Label>
                    <br />
                    <asp:DropDownList ID="op" CssClass="ddl" runat="server" Height="26px" Width="158px" Style="margin-bottom: 0">       
                        <asp:ListItem Text="Balcony" Value="44"></asp:ListItem>
                        <asp:ListItem Text="Balcony left" Value="43"></asp:ListItem>
                        <asp:ListItem Text="Balcony right" Value="42"></asp:ListItem>
                        <asp:ListItem Text="Parter" Value="41"></asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <br />
                    <asp:Button ID="check" runat="server" CssClass="btn btn-primary" OnClientClick="return jsCheck();" Text="Check" />
                    <br />
                    <br />

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="auto-style7" id="divv2">
            <asp:Label ID="LabelEmpty" Font-Size="24px" runat="server" Text=""></asp:Label>
        </div>
        <div class="auto-style7" id="divv">
            <br />
            <asp:Label ID="Price" Font-Size="24px" runat="server" ForeColor="White" Text=""></asp:Label>
            <br />
            <asp:Label ID="Hall" Font-Size="24px" runat="server" ForeColor="White" Text=""></asp:Label>
            <br />
            <asp:Label ID="Group" Font-Size="24px" runat="server" ForeColor="White" Text=""></asp:Label>
            <br />
            <asp:Label ID="SeatNr" Font-Size="24px" runat="server" ForeColor="White" Text=""></asp:Label>
            <br />
            <asp:Label ID="RowNr" Font-Size="24px" runat="server" ForeColor="White" Text=""></asp:Label>
            <br />
            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Button ID="reserve" runat="server" CssClass="btn btn-primary" OnClientClick="return jsReserve();" Text="Reserve" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="reserve" EventName="click" />

                </Triggers>
            </asp:UpdatePanel>


        </div>
    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">
        function getObj(objID) {
            return document.getElementById(objID);
        }
        <%//Function to hide reserve button %>
        function Hide() {
            document.getElementById('<%=reserve.ClientID %>').style.display = "none";
        }
            <%//Function to show reserve button %>
        function Show() {
            document.getElementById('<%=reserve.ClientID %>').style.display = 'block';
        }
            <%//Function to hide seat data %>
        function HideData() {
            document.getElementById("Price").innerHTML = "";
            document.getElementById("Hall").innerHTML = "";
            document.getElementById("Group").innerHTML = "";
            document.getElementById("SeatNr").innerHTML = "";
            document.getElementById("RowNr").innerHTML = "";
        }
        Hide();
            <%//Function to check seat %>
        function jsCheck() {
            var xmlhttp = new XMLHttpRequest();
            var rn = getObj("RowNumber").value;
            var pn = getObj("PlaceNumber").value;
            var DdlHallGroup = document.getElementById("<%=op.ClientID%>");         <% //Drop down list of calculation operations%>
            var DdlRowLetter = document.getElementById("<%=RowLetter.ClientID%>");
            var hgid = DdlHallGroup.options[DdlHallGroup.selectedIndex].value;
            var hgn = DdlHallGroup.options[DdlHallGroup.selectedIndex].text;
            var rl = DdlRowLetter.options[DdlRowLetter.selectedIndex].value;
            xmlhttp.open("GET", "Reservation.aspx?opr=check&rn=" + rn + "&pn=" + pn + "&hgid=" + hgid + "&rl=" + rl, false);
            xmlhttp.send(null);
            var response = xmlhttp.responseText;
            var status = response.split(',')[0];
            var price = response.split(',')[1];
            var HallName = response.split(',')[2];
            //Check what response was returned
            switch (status) {
                case "true":
                    document.getElementById("LabelEmpty").innerHTML = "Seat is taken";
                    document.getElementById("LabelEmpty").style.color = "red";
                    document.getElementById("divv2").style = "background-color:black;"
                    document.getElementById("divv").style = "background-color:;"
                    Hide();
                    HideData();
                    break;
                case "false":
                    document.getElementById("divv").style = "background-color:black;"
                    document.getElementById("divv2").style = "background-color:black;"
                    document.getElementById("LabelEmpty").style.color = "white";
                    document.getElementById("LabelEmpty").innerHTML = "Seat is available";
                    document.getElementById("Price").innerHTML = "Price: " + price + " €";
                    document.getElementById("Hall").innerHTML = "Hall: " + HallName;
                    document.getElementById("Group").innerHTML = "Hall group: " + hgn;
                    document.getElementById("SeatNr").innerHTML = "Seat nr: " + pn;
                    document.getElementById("RowNr").innerHTML = "Seat row: " + rn + rl;
                    Show();
                    break;
                case "NoData":
                    document.getElementById("LabelEmpty").innerHTML = "Seat is not found";
                    document.getElementById("LabelEmpty").style.color = "red";
                    document.getElementById("divv2").style = "background-color:black;"
                    document.getElementById("divv").style = "background-color:;"
                    Hide();
                    HideData();
                    break;
                case "WrongInput":
                    document.getElementById("LabelEmpty").innerHTML = "Incorrect input";
                    document.getElementById("LabelEmpty").style.color = "red";
                    document.getElementById("divv2").style = "background-color:black;"
                    document.getElementById("divv").style = "background-color:;"
                    Hide();
                    HideData();
                    break;
                case "EmptyInput":
                    document.getElementById("LabelEmpty").innerHTML = "Empty input";
                    document.getElementById("LabelEmpty").style.color = "red";
                    document.getElementById("divv2").style = "background-color:black;"
                    document.getElementById("divv").style = "background-color:;"
                    Hide();
                    HideData();
                    break;
            }

        }
            <%//Function to reserve seat %>
        function jsReserve() {
            var DdlHallGroup = document.getElementById("<%=op.ClientID%>");         <% //Drop down list of calculation operations%>
            var DdlRowLetter = document.getElementById("<%=RowLetter.ClientID%>");
            var hgid = DdlHallGroup.options[DdlHallGroup.selectedIndex].value;
            var rl = DdlRowLetter.options[DdlRowLetter.selectedIndex].value;
            var xmlhttp = new XMLHttpRequest();
            var rn = getObj("RowNumber").value;
            var pn = getObj("PlaceNumber").value;
            xmlhttp.open("GET", "Reservation.aspx?opr=reserve&rn=" + rn + "&pn=" + pn + "&hgid=" + hgid + "&rl=" + rl, false);
            xmlhttp.send(null);
            var response = xmlhttp.responseText;
            if (response == "Success") {
                document.getElementById("LabelEmpty").innerHTML = "Succesfuly reserved";
                document.getElementById("LabelEmpty").style.color = "Green";

            }
            if (response == "Failed") {
                document.getElementById("LabelEmpty").innerHTML = "Reservation failed";
                document.getElementById("LabelEmpty").style.color = "red";
            }

        }

        $.backstretch(
        [
           "img/4.jpg",
            "img/5.jpg",
            "img/6.jpg",
            "img/7.jpg",
            "img/9.jpg"
        ],

        {
            duration: 4500,
            fade: 1500
        }
    );

    </script>
</body>
</html>
