﻿<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="ajax" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Xml.aspx.cs" Inherits="Philharmony.Xml" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
    <!--[if IE 9]> <html class="no-js ie9 oldie" lang="en"> <![endif]-->
    <meta charset="utf-8">
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Philharmony</title>
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style1 {
            width: 164px;
            height: 106px;
        }

        .auto-style2 {
            width: 229px;
        }

        .auto-style3 {
            width: 41px;
        }

        .auto-style4 {
            width: 43px;
        }

        .auto-style6 {
            width: 131px;
        }
    </style>
</head>
<body>


    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Philharmony</a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/">Home</a> </li>
                        <li><a href="/Seats.aspx">Seat layout</a> </li>
                        <li><a href="/Xml.aspx">Import data</a> </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                <div id="banner">
                    <h1>
                        <strong>Philharmony seat reservation data import</strong><span class="icon-bar"></span></h1>
                </div>
            </div>
        </div>
        <div class="container">
            <table>
                <ajax:AsyncFileUpload ID="FileUpload2" runat="server"  OnClientUploadComplete="UploadComplete" OnClientUploadError="UploadError" CompleteBackColor="White" Width="350px" UploaderStyle="Modern" UploadingBackColor="#ccffff" ThrobberID="fileLoad" OnUploadedComplete="FileUpload2_UploadedComplete1" />
                <br />
                <asp:Label ID="LabelError" runat="server" ForeColor="Red"></asp:Label>
            </table>
        </div>
    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">

        $.backstretch(
        [
            "img/6.jpg",
            "img/5.jpg",
            "img/4.jpg",
            "img/7.jpg",
            "img/9.jpg"
        ],

        {
            duration: 4500,
            fade: 1500
        }
    );
        function UploadComplete() {
            document.getElementById('<%=LabelError.ClientID%>').innerHTML = "File Uploaded succesfully"
            document.getElementById('<%=LabelError.ClientID%>').style.backgroundColor = "black"
              document.getElementById('<%=LabelError.ClientID%>').style.color = "green"
        }
        function UploadError() {
            document.getElementById('<%=LabelError.ClientID%>').innerHTML = "File Upload failed"
            document.getElementById('<%=LabelError.ClientID%>').style.backgroundColor = "black"
              document.getElementById('<%=LabelError.ClientID%>').style.color = "red"
        }
    </script>
</body>
</html>
