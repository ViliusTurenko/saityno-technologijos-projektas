﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.SessionState;
using System.Web.Security;
using System.Web;
using System.Net;
using System.IO;
using System.Text;

namespace Philharmony
{
    public static class CSSHelper
    {
        public static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CSSConnectionString"].ConnectionString);      //Philharmony database connection string
        public static string tokenstring;

        public static int GetUserID()
        {
            int id = 0;
            SqlCommand cmd4 = new SqlCommand("[dbo].[User_SelectbyUserName]", con);                                      //execute stored procedure web_Event_Select with parameters

            con.Open();
            cmd4.CommandType = CommandType.StoredProcedure;
            cmd4.Parameters.Add(new SqlParameter("@Username", HttpContext.Current.User.Identity.Name));
            cmd4.ExecuteNonQuery();
            DataTable dt4 = new DataTable();
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            da4.Fill(dt4);
            if (dt4.Rows.Count > 0)
            {
                id = (int)dt4.Rows[0]["UserID"];
            }
            con.Close();
            return id;

        }

        public static int rateConspect(string conspectID, string rating)
        {
            SqlCommand cmd4 = new SqlCommand("[dbo].[Conspectus_rate]", con);                                      //execute stored procedure web_Event_Select with parameters
            con.Open();
            cmd4.CommandType = CommandType.StoredProcedure;
            cmd4.Parameters.Add(new SqlParameter("@Rate", rating));
            cmd4.Parameters.Add(new SqlParameter("@ConspectusID", conspectID));
            int x= cmd4.ExecuteNonQuery();
            con.Close();
            return x;
        }

        public static string getConspect(string userID, string conspectID)
        {
            string html = "";
            int price = 0;
            SqlCommand cmd4 = new SqlCommand("[dbo].[UserPoints_Check]", con);                                      //execute stored procedure web_Event_Select with parameters

            con.Open();
            cmd4.CommandType = CommandType.StoredProcedure;
            cmd4.Parameters.Add(new SqlParameter("@ConspectusID", conspectID));
            cmd4.Parameters.Add(new SqlParameter("@UserID", userID));
            cmd4.ExecuteNonQuery();
            DataTable dt4 = new DataTable();
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            da4.Fill(dt4);
            if (dt4.Rows.Count > 0)
            {
                html = (string)dt4.Rows[0]["HtmlText"];
            }
            con.Close();

            return html;
        }
        public static DataTable GetConspectusUsers(string conspectusID)
        {

            SqlCommand cmd4 = new SqlCommand("[dbo].[Users_SelectAssigned]", con);                                      //execute stored procedure web_Event_Select with parameters

            con.Open();
            cmd4.CommandType = CommandType.StoredProcedure;
            cmd4.Parameters.Add(new SqlParameter("@Username", HttpContext.Current.User.Identity.Name));
            cmd4.Parameters.Add(new SqlParameter("@ConspectusID", conspectusID));
            cmd4.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            da4.Fill(dt);
            con.Close();
            return dt;
        }

        public static DataTable GetUnsignedUsers(string conspectusID)
        {

            SqlCommand cmd4 = new SqlCommand("[dbo].[Users_SelectNotAssigned]", con);                                      //execute stored procedure web_Event_Select with parameters

            con.Open();
            cmd4.CommandType = CommandType.StoredProcedure;
            cmd4.Parameters.Add(new SqlParameter("@Username", HttpContext.Current.User.Identity.Name));
            cmd4.Parameters.Add(new SqlParameter("@ConspectusID", conspectusID));
            cmd4.ExecuteNonQuery();
            DataTable dt4 = new DataTable();
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            da4.Fill(dt4);
            con.Close();
            return dt4;

        }

        public static int insertPermission(string conspectusID, String pages, String userid)
        {
            SqlCommand cmd4 = new SqlCommand("[dbo].[Users_Assign]", con);                                      //execute stored procedure web_Event_Select with parameters

            con.Open();
            cmd4.CommandType = CommandType.StoredProcedure;
            cmd4.Parameters.Add(new SqlParameter("@ConspectusID", conspectusID));
            cmd4.Parameters.Add(new SqlParameter("@UserArray", pages));
            cmd4.Parameters.Add(new SqlParameter("@UserID", userid));
            int x =cmd4.ExecuteNonQuery();
            DataTable dt4 = new DataTable();
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            da4.Fill(dt4);
            con.Close();
            return x;
        }

        public static int RegisterUser(string username, string password)
        {
            string baseAddress = "http://localhost:8080";
            using (var client = new HttpClient())
            {
                var form = new Dictionary<string, string>
               {
                   {"grant_type", "password"},
                   {"userName", username},
                   {"password", password},
               };
                var tokenResponse = client.PostAsync(baseAddress + "/token", new FormUrlEncodedContent(form)).Result;
                //var token = tokenResponse.Content.ReadAsStringAsync().Result;  
                var token = tokenResponse.Content.ReadAsAsync<Token>(new[] { new JsonMediaTypeFormatter() }).Result;

                if (string.IsNullOrEmpty(token.Error))
                {
                    tokenstring = token.AccessToken;
                    FormsAuthentication.RedirectFromLoginPage(username, true);
                    return 1;
                    
                }
                else
                {
                    return -1;
                }

            }
        }
        public static string CallWebMethod(string url)
        {
            try
            {

                HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                httpRequest.Method = "GET";
                httpRequest.KeepAlive = false;
                httpRequest.ContentType = "application/json; charset=utf-8";
                httpRequest.Timeout = 30000;
                HttpWebResponse httpResponse = null;
                String response = String.Empty;
                httpRequest.Headers.Add("Authorization", "Bearer " + tokenstring);

                httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                Stream baseStream = httpResponse.GetResponseStream();
                StreamReader responseStreamReader = new StreamReader(baseStream);
                response = responseStreamReader.ReadToEnd();
                responseStreamReader.Close();

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static byte[] CreateHttpRequestData(Dictionary<string, string> dic)
        {
            StringBuilder sbParameters = new StringBuilder();
            foreach (string param in dic.Keys)
            {
                sbParameters.Append(param);//key => parameter name
                sbParameters.Append('=');
                sbParameters.Append(dic[param]);//key value
                sbParameters.Append('&');
            }
            sbParameters.Remove(sbParameters.Length - 1, 1);

            UTF8Encoding encoding = new UTF8Encoding();

            return encoding.GetBytes(sbParameters.ToString());

        }
        public static List<Conspectus> getUserConspects()
        {
            string url = "http://localhost:8080/api/Conspectus";
            string x = CSSHelper.CallWebMethod(url);
            List<Conspectus> modules = Helper.AsObjectList<Conspectus>(x);
            List<Conspectus> filteredConspectus = new List<Conspectus>();


            foreach (Conspectus cc in modules)
            {
                if (cc.UserID == CSSHelper.GetUserID())
                {
                    filteredConspectus.Add(cc);
                }
            }
            return filteredConspectus;
        }
        public static void updateUser(string username, string name, string lastname, string password, string email, string course, string facoulty)
        {
            SqlCommand cmd = new SqlCommand("[dbo].[User_update]", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@UserName", username));
            cmd.Parameters.Add(new SqlParameter("@Name", name));
            cmd.Parameters.Add(new SqlParameter("@Lastname", lastname));
            cmd.Parameters.Add(new SqlParameter("@Password", password));
            cmd.Parameters.Add(new SqlParameter("@Email",email));
            cmd.Parameters.Add(new SqlParameter("@Course", course));
            cmd.Parameters.Add(new SqlParameter("@Facoulty", facoulty));
            int x = cmd.ExecuteNonQuery();
            con.Close();
        }
        
        public static DataTable GetModuleData()
        {

            SqlCommand cmd4 = new SqlCommand("[dbo].[Module_SelectV2]", con);                                      //execute stored procedure web_Event_Select with parameters
            con.Open();
            cmd4.CommandType = CommandType.StoredProcedure;
            cmd4.ExecuteNonQuery();
            DataTable dt4 = new DataTable();
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            da4.Fill(dt4);
            con.Close();
            return dt4;


        }
        public static DataTable ConspectSearch(string moduleid,string keyword)
        {
            SqlCommand cmd4 = new SqlCommand("[dbo].[Conspectus_Search]", con);                                      //execute stored procedure web_Event_Select with parameters
            con.Open();
            cmd4.CommandType = CommandType.StoredProcedure;
            cmd4.Parameters.Add(new SqlParameter("@ModuleID", moduleid));
            cmd4.Parameters.Add(new SqlParameter("@Keyword", keyword));
            cmd4.Parameters.Add(new SqlParameter("@Username", HttpContext.Current.User.Identity.Name));
            cmd4.ExecuteNonQuery();
            DataTable dt4 = new DataTable();
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            da4.Fill(dt4);
            con.Close();
            return dt4;
        }
        public static DataTable FeelingLucky(string module, string userid)
        {
            SqlCommand cmd4 = new SqlCommand("[dbo].[Conspectus_ImLucky]", con);                                      //execute stored procedure web_Event_Select with parameters
            con.Open();
            cmd4.CommandType = CommandType.StoredProcedure;
            cmd4.Parameters.Add(new SqlParameter("@ModuleID", module));
            cmd4.Parameters.Add(new SqlParameter("@UserID", userid));
            cmd4.ExecuteNonQuery();
            DataTable dt4 = new DataTable();
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            da4.Fill(dt4);
            con.Close();
            return dt4;
        }
    }

}