﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Philharmony
{
    [Serializable]
    [XmlRoot("HallGroup")]
    public class HallGroupMetaData
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HallGroupMetaData()
        {
            this.HallSeats = new HashSet<HallSeat>();
        }
        [XmlElement("HallGroupID")]
        public int HallGroupID { get; set; }
        [XmlElement("Name")]
        public string Name { get; set; }
        [XmlElement("AZ")]
        public int AZ { get; set; }
        [XmlElement("HallID")]
        public int HallID { get; set; }
        [XmlElement]
        public virtual Hall Hall { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [XmlElement]
        public virtual ICollection<HallSeat> HallSeats { get; set; }
    }
    [MetadataType(typeof(HallGroupMetaData))]
    public partial class HallGroup
    { }

}