﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
namespace Philharmony
{
    [Serializable]
    [XmlRoot("Hall")]
    public class HallMetaData
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HallMetaData()
        {
            this.HallGroups = new HashSet<HallGroup>();
        }
        [XmlElement("HallID")]
        public int HallID { get; set; }
        [XmlElement("Name")]
        public string Name { get; set; }
        [XmlElement("TicketLimit")]
        public int TicketLimit { get; set; }
        [XmlElement]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HallGroup> HallGroups { get; set; }
    }
    [MetadataType(typeof(HallMetaData))]
    public partial class Hall
    { }
}