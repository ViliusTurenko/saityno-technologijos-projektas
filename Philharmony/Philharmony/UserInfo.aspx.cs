﻿/*UserInfo.aspx.cs
2.v1.0
3.2017/06/07
4.2017/06/08
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace Philharmony
{
    /// <summary>
    /// Class for holding methods which display user info
    /// </summary>
    public partial class UserInfo : System.Web.UI.Page
    {
        public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CssConnectionString"].ConnectionString);//Philharmony database connection string
        /// <summary>
        /// Main method for User info page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                #region if Session["userId"] is null
                if (Session["UserID"] == null)                                          //If user id is not saved to session
                {
                            Session["UserID"] = CSSHelper.GetUserID();                                    //Save user id 
                }
                #endregion
                #region get user data
                con.Open();
                SqlCommand cmd = new SqlCommand("[dbo].[User_SelectbyUserName]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Username", User.Identity.Name));
                cmd.ExecuteNonQuery();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                DataRow Dr = dt.Rows[0];
                txtEmail0.Text = (string)Dr["Email"];
                txtName0.Text = (string)Dr["Name"];
                txtLastname0.Text = (string)Dr["Lastname"];
                txtUserName2.Text = User.Identity.Name;
                txtCourse.Text = Dr["Course"].ToString();
                txtFacoulty.Text = (string)Dr["Facoulty"];
                txtPoints2.Text = (string)Dr["Points"].ToString();

                con.Close();
                #endregion
            }
        }
        /// <summary>
        /// Method for updating user information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateUser(object sender, EventArgs e)
        {
            CSSHelper.updateUser(User.Identity.Name, txtName0.Text, txtLastname0.Text, txtPassword0.Text, txtEmail0.Text, txtCourse.Text,txtFacoulty.Text);
        }
    }
}