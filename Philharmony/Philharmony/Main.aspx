﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="Philharmony.Main" EnableViewState="true" %>

<%@ Register Src="BannerTop.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
    <!--[if IE 9]> <html class="no-js ie9 oldie" lang="en"> <![endif]-->
    <meta charset="utf-8" />
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Pagrindinis</title>
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
    <style>
        .auto-style14{
            width:100%;
        }
         .auto-style7 {
            min-width: 60%;
            background: linear-gradient(to bottom, #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%); /* w3c, ie10+, ff16+, chrome26+, opera12+, safari7+ */
            border: solid;
            border-color: black
        }
        @media (min-width: 768px){
             .auto-style14{
            width:100%;
        }
        .auto-style7 {
            min-width: 250px;
            background: linear-gradient(to bottom, #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%); /* w3c, ie10+, ff16+, chrome26+, opera12+, safari7+ */
            border: solid;
            border-color: black
        }}
    </style>
</head>
<body style="font-family: 'Open Sans', sans-serif;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <header>
                    <uc1:Header ID="WebUserControl1" runat="server" />
                </header>

                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                        <div id="banner">
                            <h1>
                                <strong>Konspektų dalinimosi sistema</strong><span class="icon-bar"></span></h1>
                        </div>
                    </div>
                </div>
                <div class="auto-style7">
                    <p style="color: black; font-family:Arial; font-size:20px;">Pasirinkti modulį</p>
                    <br />
                    <asp:DropDownList ID="ddlModule" CssClass="auto-style14" BackColor="black" runat="server" Height="43px" EnableViewState="true" Style="margin-bottom: 0">
                    </asp:DropDownList>
                    <table style="width: 100%;">
                        <tr>
                            <td style="padding-left: 26%; padding-bottom: 10%;" class="auto-style13">
                                <br />
                                <asp:Button ID="check" runat="server" CssClass="btn btn-primary" OnClick="check_OnClick" Text="Man sekasi" />
                                <br />
                            </td>

                        </tr>

                    </table>
                </div>
                <div style="width: 100%; font-size: 20px;">
                    <br />
                    <table style="width: 100%; display: none;" runat="server" id="tableDatagrid">
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:TextBox runat="server" ID="ConspectCounttxt"></asp:TextBox>
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 40%">Konspekto pavadinimas</td>
                                        <td style="width: 20%">Reitingas</td>
                                        <td style="width: 20%">Kaina</td>
                                        <td style="width: 20%">Reitinguoti</td>
                                    </tr>
                                    <asp:Repeater runat="server" ID="DataGrid2">
                                        <ItemTemplate>
                                            <tr style="width: 100%">
                                                <td style="width: 40%">
                                                    <asp:LinkButton OnClientClick='<%#"OnConspectusClick("+Eval("ConspectusID")+")" %>' runat="server" ID="lbl_ConspectID" Text='<%# Eval("ConspectusName") %>'>
                                                    </asp:LinkButton></td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblRating2" runat="server" ToolTip='<%#" Balsavo : "+Eval("RatedCount") %>' Text='<%# Eval("Rating") %>'>
                                                    </asp:Label></td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblRating" runat="server" ToolTip='Taškų skaičius reikalingas peržiūrai' Text='<%# Eval("Price") %>'>
                                                    </asp:Label></td>
                                                <td style="width: 20%">
                                                    <asp:LinkButton runat="server" Enabled='<%#Eval("UserID").ToString()!=Session["UserID"].ToString() %>' OnClientClick='<%#"Saveid("+Eval("ConspectusID")+")" %>' OnClick="_1_OnClick" ID="linkbt" Text="1"></asp:LinkButton>
                                                    <asp:LinkButton runat="server" Enabled='<%#Eval("UserID").ToString()!=Session["UserID"].ToString() %>' OnClientClick='<%#"Saveid("+Eval("ConspectusID")+")" %>' OnClick="LinkButton1_OnClick" ID="LinkButton1" Text="2"></asp:LinkButton>
                                                    <asp:LinkButton runat="server" Enabled='<%#Eval("UserID").ToString()!=Session["UserID"].ToString() %>' OnClientClick='<%#"Saveid("+Eval("ConspectusID")+")" %>' OnClick="LinkButton2_OnClick" ID="LinkButton2" Text="3"></asp:LinkButton>
                                                    <asp:LinkButton runat="server" Enabled='<%#Eval("UserID").ToString()!=Session["UserID"].ToString() %>' OnClientClick='<%#"Saveid("+Eval("ConspectusID")+")" %>' OnClick="LinkButton3_OnClick" ID="LinkButton3" Text="4"></asp:LinkButton>
                                                    <asp:LinkButton runat="server" Enabled='<%#Eval("UserID").ToString()!=Session["UserID"].ToString() %>' OnClientClick='<%#"Saveid("+Eval("ConspectusID")+")" %>' OnClick="LinkButton4_OnClick" ID="LinkButton4" Text="5"></asp:LinkButton></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>

                <table>
                    <tr>
                        <td align="right"></td>
                        <td>
                            <asp:HiddenField ID="hdnUserID" Value='<%#Session["Userid"] %>' runat="server" />
                            <asp:HiddenField ID="hdnToken" Value='<%#Session["Token"] %>' runat="server" />
                            <asp:HiddenField ID="conspectusidhdn" runat="server" />
                        </td>
                    </tr>
                </table>
                </td>
</tr>
</table>
                <hr />

            </ContentTemplate>
        </asp:UpdatePanel>
        <footer>
            <uc1:Footer ID="footer1" runat="server" />
        </footer>
    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">

        $.ajaxSetup({
            beforeSend: function (xhr) {
                var h = document.getElementById('hdnToken');
                var token = h.value;
                xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            }
        });

        function OnConspectusClick(x) {
            window.open('http://localhost/conspectus.aspx?ConspectusID=' + x, 'newwin', 'height=400px,width=800px');

        }
        function showDatagrid() {
            document.getElementById('tableDatagrid').style = "display:block";
        }
        function editConspectus(x) {

            var h = document.getElementById('hdnToken');
            var token = h.value;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", "http://localhost:8080/api/Conspectus/" + x, false);
            xmlhttp.setRequestHeader('Authorization', 'Bearer ' + token);
            xmlhttp.send(null);
            var response = xmlhttp.responseText;
            var json = JSON.parse(response);
            var textbox = document.getElementById('HtmlEditorExtender1');
            var textbox2 = document.getElementById('add_ConspectusName');
            var textbox3 = document.getElementById('add_Price');
            var ConspectusID = document.getElementById('ConspectusID');
            var moduleidlbl = document.getElementById('tdConspectusID');
            var h2 = document.getElementById('add_Table');
            h2.style.display = "block";
            var e = document.getElementById("Add_Module");
            moduleidlbl.style.display = "block";
            ConspectusID.style.display = "block";
            textbox2.value = json["ConspectusName"];
            textbox3.value = json["Price"];
            ConspectusID.value = json["ConspectusID"];
            e.options[e.selectedIndex].value = json["ModuleID"];
            var htmlEditorExtender = $('.ajax__html_editor_extender_texteditor');
            htmlEditorExtender.html(json["HtmlText"]);


        }

        function Saveid(x) {
            var conidhd = document.getElementById('conspectusidhdn');
            conidhd.value = x;
        }
        function AddConspectus() {
            var UserIDhdn = document.getElementById('hdnUserID');
            var textbox = document.getElementById('TextBox1');
            var textbox2 = document.getElementById('add_ConspectusName');
            var textbox3 = document.getElementById('add_Price');
            var ConspectusIDt = document.getElementById('ConspectusID');
            var e = document.getElementById("Add_Module");
            var moduleid = e.options[e.selectedIndex].value;
            var Name = textbox2.value;
            var html = textbox.value;
            var Price = textbox3.value;
            var ConspectusID = ConspectusIDt.value;
            var userid = UserIDhdn.value;
            var url = 'http://localhost:8080/api/Conspectus';
            if (ConspectusID) {
                var Conspectus = {
                    ConspectusID: ConspectusID,
                    Price: Price,
                    ConspectusName: Name,
                    ModuleID: moduleid,
                    HtmlText: html,
                    UserID: userid
                }

                $.ajax({
                    type: "PUT",
                    url: url + "/" + ConspectusID,
                    data: JSON.stringify(Conspectus),
                    contentType: "application/json; charset=utf-8",
                    success: onSuccess,
                    error: OnErrorCall
                });
            }
            else {
                var Conspectus = {
                    ConspectusName: Name,
                    ModuleID: moduleid,
                    Price: Price,
                    HtmlText: html,
                    UserID: userid
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify(Conspectus),
                    contentType: "application/json; charset=utf-8",
                    success: onSuccess,
                    error: OnErrorCall
                });
            }

            function onSuccess(response) {
                alert('Sėkmingai pridėta');
                var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';

                if (UpdatePanel1 != null) {
                    __doPostBack(UpdatePanel1, '');
                }
            }
            function OnErrorCall(response) {
                alert(response);
            }
        }


        $.backstretch(
            [
                "img/4.jpg",
                "img/5.jpg",
                "img/6.jpg"

            ],

            {
                duration: 4500,
                fade: 1500
            }
        );


    </script>
</body>
</html>
