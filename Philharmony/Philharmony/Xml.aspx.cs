﻿/*Xml.aspx.cs
2.v1.0
3.2017/06/02
4.2017/06/02
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using AjaxControlToolkit;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;

namespace Philharmony
{/// <summary>
/// Class for handling page for xml data insertion
/// </summary>
    public partial class Xml : System.Web.UI.Page
    {
        public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["PhilharmonyConnectionString"].ConnectionString);//Database connection
        /// <summary>
        /// Main method for loading xml insertion page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            else if ((string)Session["UserType"] != "Admin")
                Response.Redirect("Reservation.aspx");
        }

        /// <summary>
        /// Method for loading xml file to database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FileUpload2_UploadedComplete1(object sender, AsyncFileUploadEventArgs e)
        {
            {
                try
                {
                    if (this.FileUpload2.PostedFile.ContentType.Equals("Application/xml") || this.FileUpload2.PostedFile.ContentType.Equals("text/xml"))
                    {
                        string fileName = Path.Combine(Server.MapPath("~/UploadDocuments"), Guid.NewGuid().ToString() + ".xml");            //Generate file path
                        FileUpload2.PostedFile.SaveAs(fileName);                                                                            //Save file
                        #region get data from xml file
                        XDocument xDoc = XDocument.Load(fileName);

                        List<Hall> hallList = xDoc.Descendants("Hall").Select(d => new Hall                                                 //Gets hall data from XDocument using Linq
                        {
                            HallID = int.Parse(d.Element("HallID").Value),                                                                  //Hall id variable
                            Name = d.Element("Name").Value,                                                                                 //Hall name variable
                            TicketLimit = int.Parse(d.Element("TicketLimit").Value),
                            EventID = 1
                        }).ToList();
                        List<HallGroup> hallGroupList = xDoc.Descendants("HallGroup").Select(d => new HallGroup                               //Gets hall group data from XDocument using Linq
                        {
                            HallGroupID = int.Parse(d.Element("HallGroupID").Value),                                                          //Hall group id variable
                            HallID = int.Parse(d.Element("HallID").Value),                                                                    //Hall id foreign key variable
                            AZ = int.Parse(d.Element("AZ").Value),                                                                            //AZ variable
                            Name = d.Element("Name").Value                                                                                    //Hall group name variable
                        }).ToList();
                        decimal price;
                        List<HallSeat> hallSeatList = xDoc.Descendants("HallSeat").Select(d => new HallSeat                                    //Gets hall seat data from XDocument using Linq  
                        {
                            ShowSeatID = int.Parse(d.Element("ShowSeatID").Value),                                                             //Seat id variable                                                           
                            HallGroupID = int.Parse(d.Element("HallGroupID").Value),                                                           //Hall group id foreign key variable
                            Color = d.Element("Color").Value,                                                                                  //Seat color variable
                            Price = decimal.TryParse(d.Element("Price").Value, out price)?price:0,                                                                   //Seat price variable
                            SeatRowLetter = d.Element("SeatRowLetter").Value,                                                                  //Seat row letter variable
                            SeatNumber = int.Parse(d.Element("SeatNumber").Value),                                                             //Seat number variable
                            SeatNumberLetter = d.Element("SeatNumberLetter").Value,                                                            //Seat number letter variable                                                
                            IsReserved = d.Element("IsReserved").Value,                                                                        //Seat vacancy variable
                            SeatRow = int.Parse(d.Element("SeatRow").Value)                                                                    //Seat row number variable

                        }).ToList();
                        #endregion
                        #region insert xml data to database
                        using (PhilharmonyDatabaseEntities dc = new PhilharmonyDatabaseEntities())
                        {
                            #region load Hall data
                            foreach (var itemHall in hallList)
                            {
                                var v = dc.Halls.Where(a => a.HallID.Equals(itemHall.HallID)).FirstOrDefault();                             //Get first record with selected id from db
                                if (v != null)                                                                                              //If id exists update data
                                {
                                    v.Name = itemHall.Name;
                                    v.TicketLimit = itemHall.TicketLimit;
                                    v.HallGroups = itemHall.HallGroups;
                                }
                                else
                                {
                                    dc.Halls.Add(itemHall);                                                                                 //If id doesnt exist, insert new record
                                }
                                dc.SaveChanges();
                            }
                            #endregion
                            #region load hall group data
                            foreach (var itemHallGroup in hallGroupList)
                            {
                                var v = dc.HallGroups.Where(a => a.HallGroupID.Equals(itemHallGroup.HallGroupID)).FirstOrDefault();//Get first record with selected id from db
                                if (v != null)                                                                                       //If id exists update data
                                {
                                    v.Name = itemHallGroup.Name;
                                    v.AZ = itemHallGroup.AZ;
                                    v.HallID = itemHallGroup.HallID;
                                    v.HallSeats = itemHallGroup.HallSeats;

                                }
                                else
                                {
                                    dc.HallGroups.Add(itemHallGroup);                                                                //If id doesnt exist, insert new record
                                }

                            }
                            dc.SaveChanges();
                            #endregion
                            #region load Hall seat data
                            foreach (var itemHallSeat in hallSeatList)
                            {
                                var v = dc.HallSeats.Where(a => a.ShowSeatID.Equals(itemHallSeat.ShowSeatID)).FirstOrDefault();     //Get first record with selected id from db
                                if (v != null)                                                                                       //If id exists update data
                                {
                                    v.ShowSeatID = itemHallSeat.ShowSeatID;
                                    v.Color = itemHallSeat.Color;
                                    v.HallGroup = itemHallSeat.HallGroup;
                                    v.HallGroupID = itemHallSeat.HallGroupID;
                                    v.IsReserved = itemHallSeat.IsReserved;
                                    v.Price = itemHallSeat.Price;
                                    v.SeatNumber = itemHallSeat.SeatNumber;
                                    v.SeatNumberLetter = itemHallSeat.SeatNumberLetter;
                                    v.SeatRow = itemHallSeat.SeatRow;
                                    v.SeatRowLetter = itemHallSeat.SeatRowLetter;
                                }
                                else
                                {

                                    dc.HallSeats.Add(itemHallSeat);                                                                  //If id doesnt exist, insert new record
                                }
                                dc.SaveChanges();
                                #endregion
                                #endregion
                            }
                         
                        }
                    }
                }
                catch(Exception ee)
                {
                    this.LabelError.Text = "Can't import xml file";
                }

            }
        }
    }
}