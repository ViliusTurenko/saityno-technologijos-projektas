﻿/*Seats.aspx.cs
2.v1.0
3.2017/05/08
4.2017/06/13
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using AjaxControlToolkit;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{/// <summary>
/// Class which holds methods for displaying seats
/// </summary>
    public partial class ConspectSearch : System.Web.UI.Page
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
            if (!Page.Request.IsAuthenticated)
            {
                Response.Redirect("/Login.aspx");
            }
            else
            {
                try { 
                hdnToken.Value = Session["Token"].ToString();

                    if (!Page.IsPostBack)
                    {

                        BindModuleDDl();

                    }
              
                hdnUserID.Value = string.IsNullOrEmpty(Session["Userid"].ToString()) ? Session["Userid"].ToString() : CSSHelper.GetUserID().ToString();
                }
                catch (NullReferenceException ee)
                {
                    FormsAuthentication.SignOut();
                    Response.Redirect("/Login.aspx");
                }
            }
            

        }
        protected void BindModuleDDl()
        {

            DataTable dt4 = CSSHelper.GetModuleData();
            filter_Module.DataSource = dt4;
            filter_Module.DataValueField = "ModuleID";
            filter_Module.DataTextField = "ModuleName";
            filter_Module.DataBind();
            Add_Module.DataSource = dt4;
            Add_Module.DataValueField = "ModuleID";
            Add_Module.DataTextField = "ModuleName";
            Add_Module.DataBind();
          


        }
        protected void BindGrid()
        {
            DataTable dt4 = CSSHelper.ConspectSearch(filter_Module.SelectedValue, filter_keyword.Text);
            if (dt4.Rows.Count == 0)
                DataGrid1.Visible = false;
            else
                DataGrid1.Visible = true;
            conspectCount2.Value = dt4.Rows.Count.ToString();
            
            DataGrid1.DataSource = dt4;
            DataGrid1.DataBind();
         
        }

        protected void HtmlEditorExtender1_ImageUploadComplete(object sender, AjaxFileUploadEventArgs e)
        {
            string filePath = "~/img/" + e.FileName;

            // Save uploaded file to the file system
            var ajaxFileUpload = (AjaxFileUpload)sender;
            ajaxFileUpload.SaveAs(MapPath(filePath));

            // Update client with saved image path
            e.PostedUrl = Page.ResolveUrl(filePath);
        }
        protected void SearchClick(object sender, EventArgs e)
        {
            BindGrid();

        }


        protected void _1_OnClick(object sender, EventArgs e)
        {
            CSSHelper.rateConspect(conspectusidhdn.Value,"1");
            conspectRate.Value = "1";
        }

        protected void LinkButton1_OnClick(object sender, EventArgs e)
        {
            CSSHelper.rateConspect(conspectusidhdn.Value, "2");
            conspectRate.Value = "2";
        }

        protected void LinkButton2_OnClick(object sender, EventArgs e)
        {
            CSSHelper.rateConspect(conspectusidhdn.Value, "3");
            conspectRate.Value = "3";
        }

        protected void LinkButton3_OnClick(object sender, EventArgs e)
        {
            CSSHelper.rateConspect(conspectusidhdn.Value, "4");
            conspectRate.Value = "4";
        }

        protected void LinkButton4_OnClick(object sender, EventArgs e)
        {
            CSSHelper.rateConspect(conspectusidhdn.Value, "5");
            conspectRate.Value = "5";
        }
    }
}