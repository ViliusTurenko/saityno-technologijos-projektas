﻿/*UserTickets.aspx.cs
2.v1.0
3.2017/06/07
4.2017/06/08
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{
    /// <summary>
    /// Class for holding methods for user ticket display
    /// </summary>
    public partial class UserTickets : System.Web.UI.Page
    {
        public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["PhilharmonyConnectionString"].ConnectionString);//Philharmony database connection string
        public DataTable dt = new DataTable();
        /// <summary>
        /// Main method for UserTickets page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                #region if Session["userId"] is null
                if (Session["UserId"] == null)                                          //If user id is not saved to session
                {
                    using (SqlCommand cmd1 = new SqlCommand("[dbo].[web_User_SelectID]"))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter())
                        {
                            cmd1.CommandType = CommandType.StoredProcedure;
                            cmd1.Parameters.AddWithValue("@Username", User.Identity.Name);
                            cmd1.Connection = con;
                            con.Open();
                            int Id = Convert.ToInt32(cmd1.ExecuteScalar());            //Get user id
                            Session["UserId"] = Id;                                    //Save user id
                            con.Close();
                        }
                    }
                }
                #endregion
            }
        }
        /// <summary>
        /// Method for gridview paging
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            con.Open();
            #region bind datasource
            SqlCommand cmd = new SqlCommand("[dbo].[web_Ticket_Select]", con);
            cmd.CommandType = CommandType.StoredProcedure;
            var StartDate = txtFromDate.Text;
            var EndDate = txtToDate.Text;
            cmd.Parameters.Add(new SqlParameter("@UserID", Session["UserID"]));
            cmd.Parameters.Add(new SqlParameter("@StartDate", StartDate));
            cmd.Parameters.Add(new SqlParameter("@EndDate", EndDate));
            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
            #endregion
            con.Close();
        }
        /// <summary>
        /// Method for filtering gridview data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void filter_Click(object sender, EventArgs e)
        {
            con.Open();
            dt.Clear();
            #region get filtered data
            SqlCommand cmd = new SqlCommand("[dbo].[web_Ticket_Select]", con);
            cmd.CommandType = CommandType.StoredProcedure;
            var StartDate = txtFromDate.Text;
            var EndDate = txtToDate.Text;
            cmd.Parameters.Add(new SqlParameter("@UserID", Session["UserID"]));
            cmd.Parameters.Add(new SqlParameter("@StartDate", StartDate));
            cmd.Parameters.Add(new SqlParameter("@EndDate", EndDate));
            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
            con.Close();
            #endregion
        }
        /// <summary>
        /// Method for handling gridview button press
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MyButtonClick(object sender, System.EventArgs e)
        {
            //Get the button that raised the event
            ImageButton btn = (ImageButton)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            DataTable dt = new DataTable();
            #region populate table
            dt.Columns.Add("Color");
            dt.Columns.Add("Price");
            dt.Columns.Add("SeatRowLetter");
            dt.Columns.Add("SeatRow");
            dt.Columns.Add("SeatNumber");
            dt.Columns.Add("ReservationDate");
            dt.Columns.Add("HallGroupName");
            dt.Columns.Add("HallName");
            dt.Columns.Add("EventName");
            dt.Columns.Add("EventCast");
            dt.Columns.Add("EventDirector");
            dt.Columns.Add("EventLength");
            dt.Columns.Add("EventDate");
            DataRow dr = dt.NewRow();
            dr["Color"] = gvr.Cells[2].Text;
            dr["Price"] = gvr.Cells[3].Text + " €";
            dr["SeatRow"] = gvr.Cells[5].Text + gvr.Cells[4].Text.Replace("&nbsp;", "");
            dr["SeatNumber"] = gvr.Cells[6].Text;
            dr["ReservationDate"] = DateTime.Parse(gvr.Cells[7].Text).ToString("yyyy/MM/dd HH:mm:ss"); 
            dr["HallGroupName"] = gvr.Cells[8].Text;
            dr["HallName"] = gvr.Cells[9].Text;
            dr["EventName"] = gvr.Cells[10].Text;
            dr["EventLength"] = gvr.Cells[11].Text + " mins";
            dr["EventCast"] = gvr.Cells[12].Text;
            dr["EventDirector"] = gvr.Cells[13].Text;
            dr["EventDate"] = DateTime.Parse(gvr.Cells[14].Text).ToString("yyyy/MM/dd HH:mm:ss");
            dt.Rows.Add(dr);
            #endregion
            Report cryRpt = new Report();                           //Crystal report instance
            cryRpt.SetDataSource(dt);                               //Set data source for report
            cryRpt.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath("Tickets/Ticket.pdf"));  //Save report in pdf format
            Response.Redirect("~/Tickets/Ticket.pdf");              //Open ticket.pdf in a new window
        }
    }

}
