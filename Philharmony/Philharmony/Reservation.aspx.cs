﻿/*Reservation.aspx.cs
2.v1.0
3.2017/05/29
4.2017/06/13
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
namespace Philharmony
{
    /// <summary>
    /// Class for Reservation web form 
    /// </summary>
    public partial class Reservation : System.Web.UI.Page
    {
        public string Opr { get; set; }     //Variable for storing user selected operation(check, reserve)
        public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["PhilharmonyConnectionString"].ConnectionString);//Philharmony DB connection string
        public int EventID { get; set; }    //Selected event id
        public int HallID { get; set; }    //Selected event id
        /// <summary>
        /// Main Reservation page method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [WebMethod(EnableSession = true)]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                #region get user type
                con.Open();
                SqlCommand cmd9 = new SqlCommand("[dbo].[web_User_Select]", con);
                cmd9.CommandType = CommandType.StoredProcedure;
                cmd9.Parameters.Add(new SqlParameter("@Username", User.Identity.Name));
                cmd9.ExecuteNonQuery();
                DataTable dtt = new DataTable();
                SqlDataAdapter daa = new SqlDataAdapter(cmd9);
                daa.Fill(dtt);
                DataRow Drr = dtt.Rows[0];
                Session["UserType"] = (string)Drr["UserType"];
                con.Close();
                #endregion
                if (!IsPostBack)
                {
                    #region populate Event drop down list
                    if (ddlEvent.Items.Count == 0)                           //If drop down list ddlHall is empty
                    {
                        con.Open();
                        SqlCommand cmd5 = new SqlCommand("Web_Event_select", con);                                           //execute stored procedure web_Hall_Select with parameters
                        cmd5.CommandType = CommandType.StoredProcedure;
                        cmd5.ExecuteNonQuery();
                        DataTable dt5 = new DataTable();
                        SqlDataAdapter da5 = new SqlDataAdapter(cmd5);
                        da5.Fill(dt5);
                        ddlEvent.DataSource = dt5;
                        ddlEvent.DataTextField = "Name";
                        ddlEvent.DataValueField = "Name";
                        ddlEvent.DataBind();                                                                                 //Databind ddlHall
                        ddlEvent.Items.Insert(0, new ListItem(String.Empty, String.Empty));                                  //Insert first empty row to ddlHall
                        con.Close();
                    }
                    LabelAvailable.Text = "";                                                                               //Reset labels
                    SeatsReserved.Text = "";
                    #endregion
                }
                #region if Session["userId"] is null
                if (Session["UserId"] == null)                                          //If user id is not saved to session
                {
                    using (SqlCommand cmd1 = new SqlCommand("[dbo].[web_User_SelectID]"))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter())
                        {
                            cmd1.CommandType = CommandType.StoredProcedure;
                            cmd1.Parameters.AddWithValue("@Username", User.Identity.Name);
                            cmd1.Connection = con;
                            con.Open();
                            int Id = Convert.ToInt32(cmd1.ExecuteScalar());            //Get user id
                            Session["UserId"] = Id;                                    //Save user id
                            con.Close();
                        }
                    }
                }
                #endregion
                #region if params exist
                if (!string.IsNullOrEmpty(Request.Params["opr"]))//If operation was selected
                {

                    Opr = Request.QueryString["opr"].ToString();//Get user selected operation
                    #region if operation is check
                    if (Opr == "check")                         //If operation is to check
                    {
                        var x = Session["HallID"];
                        if ((!string.IsNullOrEmpty(Request.Params["pn"])) && (!string.IsNullOrEmpty(Request.Params["rn"]))
                             && (!string.IsNullOrEmpty(Request.Params["hgid"])) && (!string.IsNullOrEmpty(Request.Params["ei"]))
                             && (!string.IsNullOrEmpty((string)Session["HallID"])))// if input not empty
                        {
                            if ((Regex.IsMatch(Request.Params["pn"], @"^\d+$")) && (Regex.IsMatch(Request.Params["rn"], @"^\d+$"))) //If input is numeric
                            {
                                int RowNumberCheck = int.Parse(Request.QueryString["rn"].ToString());                               //Seat row number
                                int PlaceNumberCheck = int.Parse(Request.QueryString["pn"].ToString());                             //Seat place number
                                int HallGroupIDCheck = int.Parse(Request.QueryString["hgid"].ToString());                           //Hall group id                                                                                //EventID(Prideti front ende lauka)
                                string RowLetter = Request.QueryString["rl"].ToString();                                            //Seat row letter
                                con.Open();
                                SqlCommand cmd1 = new SqlCommand("Web_HallSeat_Select", con);                                       //execute stored procedure web_HallSeat_Select with parameters
                                cmd1.CommandType = CommandType.StoredProcedure;
                                cmd1.Parameters.Add(new SqlParameter("@PlaceNumber", PlaceNumberCheck));
                                cmd1.Parameters.Add(new SqlParameter("@SeatRow", RowNumberCheck));
                                cmd1.Parameters.Add(new SqlParameter("@HallGroupID", HallGroupIDCheck));
                                cmd1.Parameters.Add(new SqlParameter("@SeatRowLetter", RowLetter));
                                cmd1.Parameters.Add(new SqlParameter("@EventID", Session["EventID"]));
                                cmd1.Parameters.Add(new SqlParameter("@HallID", Session["HallID"]));
                                cmd1.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.Output;
                                cmd1.ExecuteNonQuery();
                                int success = (int)cmd1.Parameters["@return"].Value;                                        //Get returned value
                                DataTable dt = new DataTable();
                                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                                da.Fill(dt);
                                #region if Seat not occupied
                                if (success == 0 && dt.Rows.Count > 0)                                                                               //if procedure returned more than 0 rows
                                {
                                    DataRow Dr = dt.Rows[0];
                                    if (Dr["IsReserved"].Equals("false"))                                                            //If seat is not reserved
                                    {
                                        Response.Clear();                   //Clear response to prevent wrong data 
                                        Response.Write(Dr["IsReserved"]);   //Seat occupation status
                                        Response.Write(',');
                                        Response.Write(Dr["SeatNumber"]);   //Seat occupation status
                                        Response.Write(',');
                                        Response.Write(Dr["HallGroupName"]);   //Seat occupation status
                                        Response.Write(',');
                                        Response.Write(Dr["SeatRow"]);   //Seat occupation status
                                        Response.Write(',');
                                        Response.Write(Dr["Price"]);        //Seat price
                                        Response.Write(',');
                                        Response.Write(Dr["HallName"]);     //Hall name where seat is
                                        Response.End();                     //End the response so it is sent to the client
                                    }
                                    #endregion
                                }
                                else
                                {
                                    if (success > 0)                     //If returned more than 0 seat is reserved
                                    {
                                        Response.Clear();               //Clear response to prevent wrong data 
                                        Response.Write("true");         //Seat occupation status
                                        Response.End();                 //End the response so it is sent to the client
                                    }
                                    else
                                    {
                                        Response.Clear();       //Clear response to prevent wrong data 
                                        Response.Write("NoData");//Inform that seat does not exist
                                        Response.End();         //End the response so it is sent to the client
                                    }
                                }

                            }
                            else
                            {
                                Response.Clear();            //Clear response to prevent wrong data 
                                Response.Write("WrongInput");//Inform that that wrong input was inserted
                                Response.End();              //End the response so it is sent to the client
                            }
                        }
                        else
                        {
                            Response.Clear();           //Clear response to prevent wrong data 
                            Response.Write("EmptyInput");//Inform that that input is empty
                            Response.End();             //End the response so it is sent to the client
                        }
                    }
                    #endregion
                    #region if operation is reserve
                    if (Opr == "reserve")                                       //If operation selected was to reserve seat
                    {
                        var UserID = Session["UserID"];                         //Get user id from session
                        var AdditionalInfo = "Tickets can't be returned";       //Additional info
                        if ((!string.IsNullOrEmpty(Request.Params["pn"])) && (!string.IsNullOrEmpty(Request.Params["rn"]))
                            && (!string.IsNullOrEmpty(Request.Params["hgid"])) && (!string.IsNullOrEmpty(Request.Params["ei"]))
                            && (!string.IsNullOrEmpty((string)Session["HallID"])))// if input not empty
                        {
                            var HallID = Session["HallID"];                                             //Get selected hall id from session
                            var RowNumberReservation = Request.QueryString["rn"].ToString();            //Get seat row number
                            int PlaceNumberReseration = int.Parse(Request.QueryString["pn"].ToString());//Get seat place number
                            int HallGroupID = int.Parse(Request.QueryString["hgid"].ToString());        //Get seat hall group id
                            string RowLetter = Request.QueryString["rl"].ToString();                    //Get seat row letter
                            con.Open();
                            SqlCommand cmd1 = new SqlCommand("web_HallSeat_Reserve", con);               //execute stored procedure web_HallSeat_Update
                            cmd1.CommandType = CommandType.StoredProcedure;
                            cmd1.Parameters.Add(new SqlParameter("@PlaceNumber", PlaceNumberReseration));
                            cmd1.Parameters.Add(new SqlParameter("@SeatRow", RowNumberReservation));
                            cmd1.Parameters.Add(new SqlParameter("@HallGroupID", HallGroupID));
                            cmd1.Parameters.Add(new SqlParameter("@SeatRowLetter", RowLetter));
                            cmd1.Parameters.Add(new SqlParameter("@EventID", Session["EventID"]));
                            cmd1.Parameters.Add(new SqlParameter("@UserID", UserID));
                            cmd1.Parameters.Add(new SqlParameter("@AdditionalInfo", AdditionalInfo));
                            cmd1.Parameters.Add(new SqlParameter("@HallID", Session["HallID"]));
                            cmd1.Parameters.Add("@result", SqlDbType.Int).Direction = ParameterDirection.Output;

                            cmd1.ExecuteNonQuery();
                            int success = (int)cmd1.Parameters["@result"].Value;
                            if (success == 1)                   //If stored procedure updated succesfully
                            {
                                Response.Clear();               //Clear response to prevent wrong data
                                Response.Write("Success");      //Inform about succesful update
                                Response.End();                 //End response
                            }
                            else                                   //If stored procedure update failed
                            {
                                Response.Clear();             //Clear response to prevent wrong data
                                Response.Write("Failed");     //Inform about failed update
                                Response.End();                //End response
                            }

                        }
                        con.Close();
                    }
                }

                #endregion
                #endregion
            }


        }
        /// <summary>
        /// Method for handling ddl selected value change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region populate ddlHall
            SqlCommand cmd4 = new SqlCommand("Web_Hall_Select", con);                                           //execute stored procedure web_Event_Select with parameters
            Session["EventName"] = ddlEvent.SelectedValue;                                                           //Store hall id to session
            if (!string.IsNullOrEmpty(Session["EventName"].ToString()))
            {
                con.Open();
                cmd4.Parameters.AddWithValue("Name", Session["EventName"]);
                cmd4.CommandType = CommandType.StoredProcedure;
                cmd4.ExecuteNonQuery();
                DataTable dt4 = new DataTable();
                SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
                da4.Fill(dt4);
                ddlHall.DataSource = dt4;
                ddlHall.DataTextField = "Name";
                ddlHall.DataValueField = "HallID";
                ddlHall.DataBind();                                                                                     //Bind data to ddlEvent
                ddlHall.Items.Insert(0, new ListItem(String.Empty, String.Empty));                                      //Insert first empty row to ddlEvent
                con.Close();
                LabelAvailable.Text = "";
                SeatsReserved.Text = "";
            }
            else
            {
                LabelAvailable.Text = "";
                SeatsReserved.Text = "";
            }
            #endregion
        }
        /// <summary>
        /// Method for handling ddlHall changed selected value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlHall_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["HallID"] = ddlHall.SelectedValue;
            #region get event id
            con.Open();
            SqlCommand cmd6 = new SqlCommand("web_Event_SelectEventID", con);//Stored procedure to count reserved seats 
            cmd6.CommandType = CommandType.StoredProcedure;
            cmd6.Parameters.Add("@Event", SqlDbType.Int).Direction = ParameterDirection.Output;
            var hallID = Session["HallID"];                             //Get hall id from session variable
            cmd6.Parameters.Add(new SqlParameter("@EventName", Session["EventName"]));
            cmd6.Parameters.Add(new SqlParameter("@HallID", Session["HallID"]));
            cmd6.ExecuteNonQuery();
            EventID = int.Parse(cmd6.Parameters["@Event"].Value.ToString());         //Get selected event id
            Session["EventID"] = EventID;
            #endregion
            #region count occupied and free seats

            if (!string.IsNullOrEmpty((string)Session["HallID"]))                                                 //If selected value is not the first empty one
            {
                SqlCommand cmd2 = new SqlCommand("web_HallSeats_CountReserved", con);//Stored procedure to count reserved seats 
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.Add("@Count", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd2.Parameters.Add(new SqlParameter("@EventID", EventID));
                cmd2.Parameters.Add(new SqlParameter("@HallID", hallID));
                cmd2.ExecuteNonQuery();
                var returnValue = cmd2.Parameters["@Count"].Value;          //Get stored procedure result
                SqlCommand cmd3 = new SqlCommand("web_HallSeats_Count", con);//Stored procedure to count available seats
                cmd3.CommandType = CommandType.StoredProcedure;
                cmd3.Parameters.Add("@Count", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd3.Parameters.Add(new SqlParameter("@EventID", EventID));
                cmd3.Parameters.Add(new SqlParameter("@HallID", hallID));
                cmd3.ExecuteNonQuery();
                var returnValue2 = cmd3.Parameters["@Count"].Value;

                LabelAvailable.Text = ((int)returnValue2 - (int)returnValue).ToString();              //Update label values
                SeatsReserved.Text = returnValue.ToString();

                #region populate ddlHallGroup
                SqlCommand cmd5 = new SqlCommand("web_HallGroup_Select", con);                                           //execute stored procedure web_Event_Select with parameters that returns hallgroups that belong to selected hall
                cmd5.Parameters.AddWithValue("@HallID", Session["HallID"]);
                cmd5.CommandType = CommandType.StoredProcedure;
                cmd5.ExecuteNonQuery();
                DataTable dt5 = new DataTable();
                SqlDataAdapter da5 = new SqlDataAdapter(cmd5);
                da5.Fill(dt5);
                ddlHallGroup.DataSource = dt5;
                ddlHallGroup.DataTextField = "Name";
                ddlHallGroup.DataValueField = "HallGroupID";
                ddlHallGroup.DataBind();                                                                                    //Data bind ddlHallGroup
                ddlHallGroup.Items.Insert(0, new ListItem(String.Empty, String.Empty));                                     //Insert first empty row to ddlHallGroup
                con.Close();
                #endregion
            }
            else //if selected value was the first empty one
            {
                LabelAvailable.Text = "";                                   //Reset labels
                SeatsReserved.Text = "";
            }
            #endregion
        }
    }
}