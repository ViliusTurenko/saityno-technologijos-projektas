﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="EditingPermissions.aspx.cs" Inherits="Philharmony.EditingPermissions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Šperinkis</title>
    <link href="favicon.ico" rel="Shortcut Icon" type="image/x-icon" />
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="mystyle.css">

    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style>
        .ajax__html_editor_extender_texteditor {
            background-color: white;
            color: black
        }

        TextBox1$HtmlEditorExtenderBehavior_ExtenderContainer {
            color: black;
        }

        #ajax__html_editor_extender_buttoncontainer {
            background-color: black;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Šperinkis. Sharing - is caring</a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/Main.aspx">Pagrindinis</a> </li>

                        <li><a href="/UserModules.aspx">Studentų moduliai</a> </li>
                        <li><a href="/UserConspects.aspx">Mano konspektai</a> </li>
                        <li><a href="/ConspectSearch.aspx">Konspektų paieška</a> </li>
                        <li><a href="/UserInfo.aspx">Prisijungęs kaip:
                                    <asp:LoginName ID="LoginName1" runat="server" Font-Bold="true" />
                        </a></li>
                        <li>
                            <asp:LoginStatus ID="LoginStatus1" LoginText="Prisijungti" LogoutText="Atsijungti" runat="server" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div style="margin: 0px 1000px">
                    <asp:Image ID="Image1" ImageUrl="img/loader.gif" AlternateText="Processing" runat="server" Height="60px" Width="60px" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel UpdateMode="Always" ID="UpdatePanel1" runat="server">
            <ContentTemplate>


            <asp:Literal ID="PageTitle" runat="server" Visible="false"></asp:Literal>
<table id="add_Table" cellspacing="1" cellpadding="0" width="100%" 
    border="0" runat="server">
    <tr>
        <td>
            <table class="TableStyleAdd" cellspacing="0"  cellpadding="3" width="100%" border="0">
                <tr class="TableStyleAddHeader">
                    <td align="center" colspan="3">
                        Konspekto leidimai
                        <asp:TextBox runat="server" ID="PermissionCount"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="Table2" style=" border:1px solid black; width:80%" cellpadding="1" align="center" cellspacing="0" border="0" runat="server">
                            <tr>
                                <td>
                                   Priskirti vartotojai<br />
                                    <asp:ListBox ID="selectedPages" runat="server" Rows="8" Width="100%" SelectionMode="Multiple" />
                                </td>
                                <td>
                                    <table  style="background-color:white;" border="0" cellpadding="1">
                                        <tr>
                                            <td>
                                                <input type="button" id="selectedPageOff" class="btn btn-primary" style="width:46px"  value="  >" onclick="transferPage('selected', 'all', false)"
                                                    />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="button" id="selectedPagesOff" class="btn btn-primary" value=">>" onclick="transferPage('selected', 'all', true)" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="button" id="selectedPageOn" class="btn btn-primary" style="width:46px" value="<" onclick="transferPage('all', 'selected', false)"
                                                   />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="button" id="selectedPagesOn" class="btn btn-primary" value="<<" onclick="transferPage('all', 'selected', true)" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left">
                                   Visi vartotojai
                                    <br />
                                    <asp:ListBox ID="allPages" runat="server" Rows="8" Width="100%" SelectionMode="Multiple" />
                                    <input type="hidden" id="selectedPagesString" runat="server" name="selectedPagesString" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr colspan="4">
        <td align="center">
            <asp:Button ID="insertButton2" TabIndex="1"  
                class="btn btn-primary" runat="server" OnClick="insertButton_Click"
                Text='Pridėti'></asp:Button>&nbsp;
            <input id="Close" class="btn btn-primary"
                onclick="window.close();"type="button"
                value='Uždaryti' />
        </td>
    </tr>
</table>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
<script type="text/javascript">

    var selectedPagesId = "<%= selectedPages.ClientID %>";
    var allPagesId = "<%= allPages.ClientID %>";
    var selectedPagesStringId = "<%= selectedPagesString.ClientID %>";

    function transferPage(fromName, toName, all) {
        var fromControl = document.getElementById(eval(fromName + "PagesId"));

        var toControl = document.getElementById(eval(toName + "PagesId"));
        if (all) {
            while (fromControl.childNodes.length > 0) {
                toControl.appendChild(fromControl.childNodes[0]);
            }
        } else if (!all) {
            while (fromControl.selectedIndex >= 0) {
                toControl.appendChild(fromControl.getElementsByTagName("OPTION")[fromControl.selectedIndex]);
            }
        }
     
        storePages();
        //alert('ss');
    }

    

    function storePages() {
        var elements = new Array("selected"); //"allow"
        for (var i = 0; i < elements.length; i++) {

            var options = document.getElementById(eval(elements[i] + "PagesId")).getElementsByTagName("OPTION");
            var value = "";
            if (options.length != 0) {
                for (var j = 0; j < options.length; j++) {
                    value += ((j > 0) ? ";" : "") + options[j].value;
                    //alert(value);
                    var x = document.getElementById(eval(elements[i] + "PagesStringId"));
                    x.value = value;
                }
            }
            else {
                var xx = document.getElementById(eval(elements[i] + "PagesStringId"));
                xx.value = "";
            }
        }
    }




        $.backstretch(
            [
                "img/4.jpg",
                "img/5.jpg",

            ],

            {
                duration: 4500,
                fade: 1500
            }
        );


    </script>
</body>
</html>
