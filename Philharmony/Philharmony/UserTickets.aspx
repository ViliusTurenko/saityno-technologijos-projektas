﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserTickets.aspx.cs" Inherits="Philharmony.UserTickets" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Philharmony</title>
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style7 {
            width: 428px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Philharmony</a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/Reservation.aspx">Home</a> </li>
                        <%if ((string)Session["UserType"] == "Admin")
                            { %>
                        <li><a href="/Xml.aspx">Import data</a> </li>
                        <% }%>
                        <li><a href="/UserTickets.aspx">My tickets</a> </li>
                        <li><a href="/Seats.aspx">Available seats</a> </li>
                        <li><a href="/UserInfo.aspx">Logged in as:
                                    <asp:LoginName ID="LoginName1" runat="server" Font-Bold="true" />
                        </a></li>
                        <li>
                            <asp:LoginStatus ID="LoginStatus1" runat="server" />
                        </li>


                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                <div id="banner">
                    <h1>
                        <strong>Ticket history</strong><span class="icon-bar"></span></h1>
                </div>
            </div>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upData">
            <ProgressTemplate>
                <div style="margin: 0px 1000px">
                    <asp:Image ID="Image1" ImageUrl="img/loader.gif" AlternateText="Processing" runat="server" Height="60px" Width="60px" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel runat="server" ID="upData">
            <ContentTemplate>
                <div class="container">
                    <br />
                    <div class="auto-style7">
                        From
                        <asp:TextBox type="date" ID="txtFromDate" runat="server" ForeColor="Black"></asp:TextBox>To<asp:TextBox type="date" ID="txtToDate" runat="server" ForeColor="Black"></asp:TextBox>
                        <asp:Button ID="btnFilter" runat="server" CssClass="btn btn-primary" OnClick="filter_Click" Text="Show" />
                    </div>
                    <asp:GridView ID="GridView1" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                        AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="10">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Button1" Width="30px" Height="30px" runat="server" ToolTip="Print ticket" ImageUrl="img/print.png"
                                        OnClick="MyButtonClick" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" />
                        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                        <RowStyle BackColor="DarkGray" ForeColor="White" />
                        <AlternatingRowStyle BackColor="Gray" ForeColor="White" />
                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#808080" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#383838" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery.backstretch.js" type="text/javascript"></script>
        <script type="text/javascript">
            $.backstretch(
            [
                "img/6.jpg",
                "img/5.jpg",
                "img/4.jpg",
                "img/7.jpg",
                "img/9.jpg"
            ],

            {
                duration: 4500,
                fade: 1500
            }
        );
        </script>


    </form>
</body>
</html>

