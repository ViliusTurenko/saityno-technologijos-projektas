﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Philharmony.Login" %>

<%@ Register Src="Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Prisijungimas</title>
    <link href="favicon.ico" rel="Shortcut Icon" type="image/x-icon" />
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="mystyle.css">

    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style>
          .logindiv{
               padding-left: 5%;
                padding-bottom: 10%;
                height: 20%;
                width: 100%;
                align-items: center
            }
        @media (min-width: 768px) {
            .logindiv{
                padding-left: 50%;
                padding-bottom: 20%;
                height: 200px;
                width: 200px;
                align-items: center
            }
        }
    </style>
</head>
<body style="font-family: 'Open Sans', sans-serif;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Šperinkis. Sharing - is caring</a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/Registration.aspx"><img border="0" alt="Registracija" title="Registracija" src="img/note.svg" width="50" height="50"/></a> </li>
                        <li><a href="/UserInfo.aspx">
                            <asp:Label runat="server" Text="Prisijungęs kaip:" Visible='false' ID="loginNamelbl"></asp:Label>
                            <asp:LoginName ID="LoginName1" runat="server" Font-Bold="true" />
                        </a></li>
                        <li>
                          <asp:LoginStatus ID="LoginStatus1" LoginImageUrl="img/sign-in.svg" Width="50"  Height="50" LogoutImageUrl="img/login (1).svg" runat="server" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div style="position: absolute; text-align: center">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div style="margin: 0px 1000px">
                        <asp:Image ID="Image1" ImageUrl="img/loader.gif" AlternateText="Processing" runat="server" Height="60px" Width="60px" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <asp:UpdatePanel UpdateMode="Always" ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                        <div id="banner">
                            <h1>
                                <strong>Prisijungimas prie sistemos</strong><span class="icon-bar"></span></h1>
                        </div>
                    </div>
                </div>
                <div class="logindiv">


                    <%= "Vartotojo vardas" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;
                        
                            <asp:TextBox ID="add_Username" Style="background-color: #FEFDEE; width: 200px; font-size: 11px; border: solid 1px #74BAF3; color: black;" runat="server" Width="200px" Columns="40"></asp:TextBox>


                    <br />
                    <br />
                    <%= "Slaptažodis" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;
                        
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp   
                    <asp:TextBox ID="add_password" TextMode="password" Style="background-color: #FEFDEE; width: 200px; height:30px; font-size: 11px; border: solid 1px #74BAF3; color: black;" runat="server" Columns="40"></asp:TextBox>




                    </br>
                    </br>
                    <asp:TextBox runat="server" ID="txtError" Text=""></asp:TextBox>
                        <div>
                            <asp:ImageButton Width="70" Height="50" ToolTip="Prisijungti" ImageUrl="img/logins.svg" ID="btnReg" runat="server" OnClick="RegisterUser" />
                        </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <hr />
        <footer>
            <uc1:Footer ID="footer1" runat="server" />
        </footer>
    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">


        $.backstretch(
            [
                "img/4.jpg",
                "img/5.jpg",
                "img/6.jpg"

            ],

            {
                duration: 4500,
                fade: 1500
            }
        );

    </script>
</body>
</html>
