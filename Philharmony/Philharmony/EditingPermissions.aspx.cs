﻿/*Seats.aspx.cs
2.v1.0
3.2017/05/08
4.2017/06/13
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using AjaxControlToolkit;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{/// <summary>
/// Class which holds methods for displaying seats
/// </summary>
    public partial class EditingPermissions : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                DataBind();           
            }
        }
 
        public override void DataBind()
        {
            base.DataBind();
            try
            {
                DataTable dt = CSSHelper.GetUnsignedUsers(qConspectusID);
                allPages.Items.Clear();
                selectedPages.Items.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    allPages.Items.Add(new ListItem((string)row["Username"], row["UserID"].ToString()));
                }
                allPages.Focus();
            }
            catch (Exception) { }

            if (qConspectusID != "0")
            {
                try
                {
                    DataTable dt2 = CSSHelper.GetConspectusUsers(qConspectusID);
                    foreach (DataRow row in dt2.Rows)
                    {
                        allPages.Items.Remove(new ListItem((string)row["Username"], row["UserID"].ToString()));
                    }
                    selectedPages.Items.Clear();
                    foreach (DataRow row in dt2.Rows)
                    {
                        selectedPages.Items.Add(new ListItem((string)row["Username"], row["UserID"].ToString()));
                        PermissionCount.Text = selectedPages.Items.Count.ToString();
                    }
                }
                catch (Exception) { }
            }
        }

       



        protected void insertButton_Click(object sender, EventArgs e)
        {
            CSSHelper.insertPermission(qConspectusID, selectedPagesString.Value, Session["UserID"].ToString());

        }



        public string qConspectusID
        {
            get { return Request.QueryString["ConspectusID"]; }
        }
    }
    
}