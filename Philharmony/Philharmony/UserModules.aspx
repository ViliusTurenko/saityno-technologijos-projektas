﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserModules.aspx.cs" Inherits="Philharmony.UserModules" %>

<%@ Register Src="BannerTop.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Šperinkis</title>
    <link href="favicon.ico" rel="Shortcut Icon" type="image/x-icon" />
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="mystyle.css"/>

    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style>
          .textboxAdd {
                background-color: #FEFDEE;
                font-size: 11px;
                border: solid 1px #74BAF3;
                color: black;
                width:80%;
            }
        @media (min-width: 768px) {
            .textboxAdd {
                background-color:black;
                font-size: 11px;
                border: solid 1px #74BAF3;
                color: black;
                width:200px;
            }


        }
    </style>
</head>
<body style="font-family: 'Open Sans', sans-serif;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <header>
            <uc1:Header ID="WebUserControl1" runat="server" />
        </header>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div style="margin: 0px 0px">
                    <asp:Image ID="Image1" ImageUrl="img/loader.gif" AlternateText="Processing" runat="server" Height="60px" Width="60px" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel UpdateMode="Always" ID="UpdatePanel1" runat="server">
            <ContentTemplate>


                <table class="TableBorder" style="width:100%" border="0">

                    <tr>
                        <td>
                            <asp:TextBox ID="txtModuleCount" runat="server"></asp:TextBox>
                            <asp:DataGrid ID="DataGrid1" Style=" font-size: 16px; width:95%; max-width:1600px; height:auto; " CellPadding="3" PageSize="20"
                                 AllowSorting="True" AutoGenerateColumns="False" GridLines="none"  CellSpacing="1"
                                runat="server">

                                <Columns>
                                    <asp:TemplateColumn>
                                        <ItemStyle Width="2%"></ItemStyle>
                                       
                                        <ItemTemplate>
                                            <asp:ImageButton  Width="35" Height="35" ImageUrl="img/006-eraser.svg" Visible='<%#hdnUserID.Value==Eval("UserID").ToString() %>' runat="server" ToolTip="Trinti" OnClientClick='<%#"DeleteModule("+Eval("ModuleID").ToString()+")"%>' ID="deleteButton" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemStyle Width="2%"></ItemStyle>
                                        
                                        <ItemTemplate>
                                            <asp:ImageButton  Width="35" Height="35" ImageUrl="img/005-pencil.svg"   runat="server" Visible='<%#hdnUserID.Value==Eval("UserID").ToString() %>'  ToolTip="Redaguoti"  OnClientClick='<%#"editModule("+Eval("ModuleID").ToString()+")"%>' ID="editButton" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>

                                            <%# "Modulio pavadinimas"%>
                                        </HeaderTemplate>
                                        <ItemStyle CssClass="TableStyleCell1" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ModuleName" runat="server" Text='<%# Eval("ModuleName") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="edit_ModuleName" CssClass="validInputRequired" runat="server" Text='<%# Eval("ModuleName") %>'>
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="validationWarningSmall" ForeColor=" "
                                                ErrorMessage="*" ControlToValidate="edit_ModuleName" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <%# "Semestras" %>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSemester" runat="server" Text='<%# Eval("Semester") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="edit_Semester" CssClass="validInputRequired" runat="server" Text='<%# Eval("Semester") %>'>
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="validationWarningSmall" ForeColor=" "
                                                ErrorMessage="*" ControlToValidate="edit_ModuleName" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <%# "Fakultetas" %>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFacoulty" runat="server" Text='<%# Eval("Facoulty") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="edit_Facoulty" CssClass="validInputRequired" runat="server" Text='<%# Eval("Facoulty") %>'>
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="validationWarningSmall" ForeColor=" "
                                                ErrorMessage="*" ControlToValidate="edit_Facoulty" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Position="TopAndBottom" HorizontalAlign="Left" PageButtonCount="20"
                                    Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                            <asp:HiddenField ID="hdnToken" Value='<%#Session["Token"] %>' runat="server" />
                            <asp:HiddenField ID="hdnUserID" Value='<%#Session["UserID"] %>' runat="server" />
                        </td>
                    </tr>
                </table>
                <br>

                <a name="Edit"></a>
                <table id="add_Table" cellspacing="1" cellpadding="0" width="100%"
                    border="0" runat="server">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="3" width="100%" border="0">
                                <tr>
                                    <td align="center" colspan="2"><%= "Modulio redagavimas/pridėjimas" %></td>
                                </tr>
                                <tr>
                                    <td id="tdModuleID" style="display: none" align="right"><%= "Modulio id :" %></td>
                                    <td>

                                        <asp:TextBox ID="ModuleID" Enabled="false" Style="display: none; background-color: #FEFDEE; font-size: 11px; border: solid 1px #74BAF3; color: black;" runat="server" Columns="40"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="right"><%= "Modulio pavadinimas" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;</td>
                                    <td>
                                        <asp:HiddenField ID="HdnModuleID" runat="server" />
                                        <asp:TextBox ID="add_ModuleName" CssClass="textboxAdd"  runat="server"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" CssClass="validationWarningSmall" runat="server" ForeColor=" "
                                            ErrorMessage="*" ControlToValidate="add_ModuleName" ValidationGroup="add" Display="Static"></asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="right"><%= "Semestras" %>&nbsp;(<span class="RequiredFieldMark">*</span>):&nbsp;</td>
                                    <td>
                                        <asp:TextBox ID="add_Semester" CssClass="textboxAdd"  runat="server" ></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="validationWarningSmall" runat="server" ForeColor=" "
                                            ErrorMessage="*" ValidationGroup="add" ControlToValidate="add_Semester" Display="Static"></asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="right"><%= "Fakultetas" %> : </td>
                                    <td>
                                        <asp:TextBox ID="add_Facoulty" ValidationGroup="add" CssClass="textboxAdd"  runat="server" ></asp:TextBox>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td align="right">  <asp:TextBox ID="ModuleName" CssClass="textboxAdd"  runat="server"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="validationWarningSmall" runat="server" ForeColor=" "
                                            ErrorMessage="*" ControlToValidate="add_ModuleName" ValidationGroup="add" Display="Static"></asp:RequiredFieldValidator></td></td>
                                    <td> 
                                        <asp:Button Width="70" Height="50"  ID="insertButton3"
                                            runat="server" ValidationGroup="add" OnClientClick='javascript:AddModule()' Tooltip='Išsaugoti'></asp:Button>
                                        <asp:ImageButton ID="Button"  Width="70" Height="50" ImageUrl="img/cancel.svg"
                                            OnClientClick="javascript:window.location.href='UserModules.aspx'; return false;" runat="server"
                                            Tooltip='Atšaukti'></asp:ImageButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <hr />
                    <footer>
                        <uc1:Footer ID="footer1" runat="server" />
                    </footer>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">

        $.ajaxSetup({
            beforeSend: function (xhr) {
                var h = document.getElementById('hdnToken');
                var token = h.value;
                xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            }
        });

        function DeleteModule(x) {
            var h = document.getElementById('hdnToken');
            var token = h.value;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("DELETE", "http://localhost:8080/api/Modules/" + x, false);
            xmlhttp.setRequestHeader('Authorization', 'Bearer ' + token);
            xmlhttp.send(null);
            var response = xmlhttp.responseText;
            location.reload();
        }


        function editModule(x) {
            var h = document.getElementById('hdnToken');
            var token = h.value;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", "http://localhost:8080/api/Modules/" + x, false);
            xmlhttp.setRequestHeader('Authorization', 'Bearer ' + token);
            xmlhttp.send(false);
            var response = xmlhttp.responseText;
            var json = JSON.parse(response);
            var textbox = document.getElementById('add_ModuleName');
            var textbox2 = document.getElementById('add_Facoulty');
            var textbox3 = document.getElementById('add_Semester');
            var ModuleID = document.getElementById('ModuleID');
            var moduleidlbl = document.getElementById('tdModuleID');
            var ModuleIDtxt = document.getElementById('ModuleName');
            ModuleIDtxt.value = "Objektinis programavimas5";
            moduleidlbl.style.display = "block";
            ModuleID.style.display = "block";
            textbox.value = json["ModuleName"];
            textbox2.value = json["Facoulty"];
            textbox3.value = json["Semester"];
            ModuleID.value = json["ModuleID"];

        }


        function AddModule() {

            var textbox = document.getElementById('add_ModuleName');
            var textbox2 = document.getElementById('add_Facoulty');
            var textbox3 = document.getElementById('add_Semester');
        
            var ModuleIDtxt = document.getElementById('ModuleID');
            var UserIDhdn = document.getElementById('hdnUserID');
            var moduleName = textbox.value;
            var Facoulty = textbox2.value;
            var Semester = textbox3.value;
           
            var ModuleID = ModuleIDtxt.value;
            var userid = UserIDhdn.value;
            var url = "http://localhost:8080/api/Modules";
            if (ModuleID) {
                var module = {
                    ModuleID: ModuleID,
                    ModuleName: moduleName,
                    Facoulty: Facoulty,
                    Semester: Semester,
                    UserID: userid
                }

                $.ajax({

                    type: "PUT",
                    url: url + "/" + ModuleID,
                    data: JSON.stringify(module),
                    contentType: "application/json; charset=utf-8",
                    success: onSuccess,
                    error: OnErrorCall
                });
            }
            else {
                var module = {
                    ModuleName: moduleName,
                    Facoulty: Facoulty,
                    Semester: Semester,
                    UserID: userid
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify(module),
                    contentType: "application/json; charset=utf-8",
                    success: onSuccess,
                    error: OnErrorCall
                });
            }

            function onSuccess(response) {
                var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';

                if (UpdatePanel1 != null) {
                  
                }

                
            }
            function OnErrorCall(response) {
                alert(response);
            }
        }


        $.backstretch(
            [
                "img/4.jpg",
                "img/5.jpg",
                "img/6.jpg"

            ],

            {
                duration: 4500,
                fade: 1500
            }
        );


    </script>
</body>
</html>
