﻿/*Seats.aspx.cs
2.v1.0
3.2017/05/08
4.2017/06/13
5.Vilius
6.Seat reservation system
7.Autorinės teisės priklauso Vilius Turenko
8.-
9.-
10.-*/
using AjaxControlToolkit;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Philharmony
{/// <summary>
/// Class which holds methods for displaying seats
/// </summary>
    public partial class UserConspects : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.Request.IsAuthenticated)
            {
                Response.Redirect("/Login.aspx");
            }
            else
            {
                try
                {
                    hdnToken.Value = CSSHelper.tokenstring; /*Session["Token"].ToString();*/
                    BindModuleDDl();
                    BindGrid();
                    hdnUserID.Value = string.IsNullOrEmpty(Session["Userid"].ToString()) ? Session["Userid"].ToString() : CSSHelper.GetUserID().ToString();
                }
                catch (NullReferenceException ee)
                {
                    FormsAuthentication.SignOut();
                    Response.Redirect("/Login.aspx");
                }
            }


        }

        protected void BindGrid()
        {
            ConspectCount.Text = CSSHelper.getUserConspects().Count.ToString();
            DataGrid1.DataSource = CSSHelper.getUserConspects();
            DataGrid1.DataBind();

        }
       
        protected void BindModuleDDl()
        {
            Add_Module.DataSource = CSSHelper.GetModuleData();
            Add_Module.DataValueField = "ModuleID";
            Add_Module.DataTextField = "ModuleName";
            Add_Module.DataBind();


        }
       
        protected void HtmlEditorExtender1_ImageUploadComplete(object sender, AjaxFileUploadEventArgs e)
        {
            string filePath = "~/img/" + e.FileName;

            // Save uploaded file to the file system
            var ajaxFileUpload = (AjaxFileUpload)sender;
            ajaxFileUpload.SaveAs(MapPath(filePath));

            // Update client with saved image path
            e.PostedUrl = Page.ResolveUrl(filePath);
        }


    }
}