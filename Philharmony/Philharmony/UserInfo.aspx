﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserInfo.aspx.cs" Inherits="Philharmony.UserInfo" %>

<%@ Register Src="BannerTop.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
    <!--[if IE 9]> <html class="no-js ie9 oldie" lang="en"> <![endif]-->
    <meta charset="utf-8" />
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Vartotojo informacija</title>
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet'
        type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800'
        rel='stylesheet' type='text/css' />
    <!-- ============ Add custom CSS here ============ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            margin-left: auto;
            margin-right: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .auto-style2 {
            width: 50%;
        }

        .auto-style3 {
            width: 70%;
            margin-left: 0px;
        }

        .auto-style4 {
            width: 20%;
        }
    </style>
</head>
<body>


    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
        <header>
            <uc1:Header ID="WebUserControl1" runat="server" />
        </header>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="up">
            <ProgressTemplate>
                <div style="margin-left: 160px">
                    <asp:Image ID="Image1" ImageUrl="img/loader.gif" AlternateText="Processing" runat="server" />
                </div>

            </ProgressTemplate>
        </asp:UpdateProgress>

        <asp:UpdatePanel ID="up" runat="server">
            <ContentTemplate>
                <div class="auto-style1">
                    <table class="auto-style3">
                        <tr>
                            <th colspan="3">
                                <asp:Label ID="Label2" runat="server" Text="">Informacija vartotojui:</asp:Label>
                                <asp:Label ID="txtUserName2" runat="server" ReadOnly="true" />
                            </th>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Taškų kiekis</td>
                            <td>
                                <asp:Label ID="txtPoints2" ToolTip="Turimas taškų kiekis" runat="server" ReadOnly="true" /></td>
                        </tr>
                        <tr>
                            <td class="auto-style4">
                                <asp:Label ID="Label3" runat="server" Text="">Vardas</asp:Label>
                            </td>
                            <td class="auto-style4">
                                <asp:TextBox ID="txtName0" runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:RequiredFieldValidator ErrorMessage="Privaloma" ForeColor="Red" ControlToValidate="txtName0"
                                    runat="server" />
                            </td>

                        </tr>
                        <tr>
                            <td class="auto-style2">
                                <asp:Label ID="Label4" runat="server" Text="">Pavardė</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtLastname0" runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:RequiredFieldValidator ErrorMessage="Privaloma" ForeColor="Red" ControlToValidate="txtLastname0"
                                    runat="server" />
                            </td>

                        </tr>

                        <tr>
                            <td class="auto-style2">
                                <asp:Label ID="Label5" runat="server" Text="">El paštas</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail0" runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:RequiredFieldValidator ErrorMessage="Privaloma" Display="Dynamic" ForeColor="Red"
                                    ControlToValidate="txtEmail0" runat="server" />
                                <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ControlToValidate="txtEmail0" ForeColor="Red" ErrorMessage="Netinkamas el pašto adreso formatas" />
                            </td>

                        </tr>
                        <tr>
                            <td class="auto-style2">
                                <asp:Label ID="Label7" runat="server" Text="">Kursas</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCourse" runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Privaloma" Display="Dynamic" ForeColor="Red"
                                    ControlToValidate="txtCourse" runat="server" />

                            </td>

                        </tr>
                        <tr>
                            <td class="auto-style2">
                                <asp:Label ID="Label8" runat="server" Text="">Fakultetas</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFacoulty" runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Privaloma" Display="Dynamic" ForeColor="Red"
                                    ControlToValidate="txtFacoulty" runat="server" />

                            </td>

                        </tr>
                        <tr>
                            <td class="auto-style4">
                                <asp:Label ID="Label1" runat="server" Text="">Slaptažodis</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPassword0" ForeColor="black" TextMode="Password" runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:RequiredFieldValidator ErrorMessage="Privaloma" ForeColor="Red" ControlToValidate="txtPassword0"
                                    runat="server" />
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label6" runat="server" Text="">Pakartoti slaptažodį</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtConfirmPassword" ForeColor="black" runat="server" TextMode="Password" />
                            </td>
                            <td class="auto-style9">
                                <asp:CompareValidator ErrorMessage="Slaptažodžiai nesutampa" ForeColor="Red" ControlToCompare="txtPassword0"
                                    ControlToValidate="txtConfirmPassword" runat="server" />
                            </td>

                        </tr>

                        <tr>
                            <td class="auto-style4"></td>
                            <td>
                                <asp:HiddenField runat="server" ID="passwordstatus" />
                                <asp:Button CssClass="btn btn-primary" ID="btnUpdate" Text="Išsaugoti" runat="server" OnClientClick="check();" OnClick="UpdateUser" />
                            </td>
                            <td class="auto-style2"></td>

                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <hr />
        <footer>
            <uc1:Footer ID="footer1" runat="server" />
        </footer>
    </form>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.backstretch.js" type="text/javascript"></script>
    <script type="text/javascript">

        function check() {
            var xx = document.getElementById('txtPassword0');
            var xxx = document.getElementById('txtConfirmPassword');

            var y = xx.value;
            var y2 = xxx.value;
            if (y != y2) {
                var x = document.getElementById('passwordstatus');
                x.value = "Nesutampa slaptažodžiai";
                
            }
            else {
                var x = document.getElementById('passwordstatus');
                x.value = "Yra neužpildytų laukų";

            }
            var xx = document.getElementById('txtUserName');
            if (!xx.value)
            {
                var x = document.getElementById('passwordstatus');
                x.value = "Yra neužpildytų laukų";
            }
            return true;
        }
        $.backstretch(
            [
                "img/4.jpg",
                "img/5.jpg",
                "img/6.jpg"

            ],

            {
                duration: 4500,
                fade: 1500
            }
        );

    </script>
</body>
</html>
