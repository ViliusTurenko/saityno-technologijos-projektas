﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OwinSample;
using System.Web.Http.Cors;

namespace OwinSample.Service
{
    [EnableCors(origins: "http://localhost", headers: "*", methods: "*")]
    public class ConspectusController : ApiController
    {
    
        private CSSDatabaseEntities db = new CSSDatabaseEntities();
        [Authorize]
        // GET: api/Conspectus
        public IQueryable<Conspectus> GetConspectus()
        {
            return db.Conspectus;
        }
        [Authorize]
        // GET: api/Conspectus/5
        [ResponseType(typeof(Conspectus))]
        public IHttpActionResult GetConspectus(int id)
        {
            Conspectus conspectus = db.Conspectus.Find(id);
            if (conspectus == null)
            {
                return NotFound();
            }
            conspectus.HtmlText = System.Web.HttpUtility.HtmlDecode(conspectus.HtmlText);
            return Ok(conspectus);
        }
        [Authorize]
        // PUT: api/Conspectus/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutConspectus(int id, Conspectus conspectus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != conspectus.ConspectusID)
            {
                return BadRequest();
            }

            db.Entry(conspectus).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConspectusExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/Conspectus
        [ResponseType(typeof(Conspectus))]
        public IHttpActionResult PostConspectus(Conspectus conspectus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Conspectus.Add(conspectus);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = conspectus.ConspectusID }, conspectus);
        }
        [Authorize]
        // DELETE: api/Conspectus/5
        [ResponseType(typeof(Conspectus))]
        public IHttpActionResult DeleteConspectus(int id)
        {
            Conspectus conspectus = db.Conspectus.Find(id);
            if (conspectus == null)
            {
                return NotFound();
            }

            db.Conspectus.Remove(conspectus);
            db.SaveChanges();

            return Ok(conspectus);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool ConspectusExists(int id)
        {
            return db.Conspectus.Count(e => e.ConspectusID == id) > 0;
        }
    }
}