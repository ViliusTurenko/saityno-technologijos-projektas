﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OwinSample;
using System.Web.Http.Cors;

namespace OwinSample.Service
{
    
    [EnableCors(origins: "http://localhost", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {
        private CSSDatabaseEntities db = new CSSDatabaseEntities();
        [AllowAnonymous]
        public User GetUserByCredentials(string email, string password)
        {

            bool exists = (from d in db.User where d.Username == email && d.Password == password select new { d.Email, d.Password }).Any();
            
               if (!exists) 
            {
                return null;
            }
            User user = new User() { UserID = 1, Email = email, Password = password, Name = "Vilius" };
            if (user != null)
            {
                user.Password = string.Empty;
            }
            return user;
        }

        // GET: api/Users
        public IQueryable<User> GetUser()
        {
            return db.User;
        }

        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(int id)
        {
            User user = db.User.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserID)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [AllowAnonymous]
        // POST: api/Users
        [ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {
            user.Points = 50;
            user.CreatedDate = System.DateTime.Now;
            user.UserType = "client";
        bool exists = (from d in db.User where d.Username == user.Username || d.Email == user.Email select new { d.Email, d.Password }).Any();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (!exists) { 
            db.User.Add(user);
            db.SaveChanges();
           
            return CreatedAtRoute("DefaultApi", new { id = user.UserID }, user);
            }
            return null;
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = db.User.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.User.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.User.Count(e => e.UserID == id) > 0;
        }
    }
}